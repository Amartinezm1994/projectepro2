#include <map>
#include <vector>
#include <string>
#include <set>
#include <sstream>

#include "Text.hh"
#include "AuxiliarsGenerals.hh"

using namespace std;

bool Text::ComparacioParaules::operator() (const string &p1, const string &p2) const
{
    return p1.size() < p2.size() or (p1.size() == p2.size() and p1 < p2);
}

void Text::afegir_paraula_taula_frequencies(const string &p)
{
	map<int,set<string,ComparacioParaules> >::iterator it = frequencies.begin();
	// Esquema de cerca
	bool trobat = false;
	set<string,ComparacioParaules>::iterator itset;
	/* Sigui X1, X2, ..., Xn el nombre d'elements dels sets del contingut 
		dels diccionaris (els sets), el cost és:
			Log2(X1) + Log2(X2) + ... + Log2(Xn)
	*/
	while(not trobat and it != frequencies.end()) {
		// Cost Log2(n) respecte els elements d'aquest set
		itset = it->second.find(p);
		trobat = itset != it->second.end();
		++it;
	}
	if (trobat) {
		--it;
		// Cost constant
		it->second.erase(itset);
		int aparicions = it->first + 1;
		if (it->second.empty()) {
			// Cost constant
			it = frequencies.erase(it);
		}
		it = frequencies.find(aparicions);
		if (it != frequencies.end()) {
			it->second.insert(p);
		} else {
			set<string,ComparacioParaules> ss;
			ss.insert(p);
			frequencies.insert(make_pair(aparicions, ss));
		}
	} else {
		it = frequencies.begin();
		if (not frequencies.empty() and it->first == 1) {
			it->second.insert(p);
		} else {
			set<string,ComparacioParaules> ss;
			ss.insert(p);
			// Cost constant
			frequencies.insert(it, make_pair(1, ss));
		}
	}
}



void Text::gestiona_paraula(string &paraula, Frase &fr, istringstream &iss)
{
    bool puntuacio = es_puntuacio(paraula.back());
    bool puntuacio_nomes = puntuacio and paraula.size() == 1;
    bool doble_puntuacio = puntuacio and paraula.size() >= 2 and es_puntuacio(paraula[paraula.size() - 2]);
    bool doble_puntuacio_nomes = doble_puntuacio and paraula.size() == 2;
    if (not puntuacio_nomes and not doble_puntuacio_nomes) {
		fr.afegir_paraula(string(1, ' '));
	}
    string back = "";
	if (puntuacio) {
        back += paraula.back();
        paraula.pop_back();
        if (doble_puntuacio) {
            char aux = back[0];
            back[0] = paraula.back();
            back += aux;
            paraula.pop_back();

        }
	}
    if (not puntuacio_nomes and not doble_puntuacio_nomes) {
		fr.afegir_paraula(paraula);
		afegir_paraula_taula_frequencies(paraula);
		++num_paraules;
		
	}
	if (puntuacio) {
        fr.afegir_paraula(back);
	}
	ws(iss);
}

Text::Text(string &text, const string &titol, const string &autor)
{
	this->titol = titol;
	this->autor = autor;
	num_paraules = 0;
	frases = vector<Frase>();
	cites = set<pair<string,int> >();
	frequencies = map<int,set<string,ComparacioParaules> >();
    istringstream iss(text);
    char last = iss.peek();
    while (last != -1) {
    	Frase fr;
    	string paraula;
    	iss >> paraula;
    	while (last != -1 and paraula.back() != '.' and paraula.back() != '!' and paraula.back() != '?') {
    		gestiona_paraula(paraula, fr, iss);
    		last = iss.peek();
			if (last != -1)
				iss >> paraula;
    	}
    	if (last != -1) {
    		gestiona_paraula(paraula, fr, iss);
    		last = iss.peek();
    	}
    	fr.elimina_espai_inicial();
    	frases.push_back(fr);

    }
}

Text::~Text(){}

void Text::substitueix(const string &p1, const string &p2)
{
	map<int,set<string,ComparacioParaules> >::iterator it = frequencies.begin();
	map<int,set<string,ComparacioParaules> >::iterator it1, it2;
	set<string,ComparacioParaules>::iterator it1_s, it2_s;
	bool trobada = false, trobada1 = false, trobada2 = false;
	while(not trobada and it != frequencies.end()) {
		set<string,ComparacioParaules>::iterator it_s;
		if (not trobada1) {
			it_s = it->second.find(p1);
			trobada1 = it_s != it->second.end();
			if (trobada1) {
				it1_s = it_s;
				it1 = it;
				trobada = trobada1 and trobada2;
			}
		}
		if (not trobada2) {
			it_s = it->second.find(p2);
			trobada2 = it_s != it->second.end();
			if (trobada2) {
				it2_s = it_s;
				it2 = it;
				trobada = trobada1 and trobada2;
			}
		}
		++it;
	}
	// Si només troba p2 no té sentit substituir res.
	if (trobada1) {
		int num_p1 = it1->first;
		it1->second.erase(it1_s);
		if (it1->second.empty()) {
			frequencies.erase(it1);
		}
		if (trobada) {
			int num_p2 = num_p1 + it2->first;
			it2->second.erase(it2_s);
			if (it2->second.empty()) {
				frequencies.erase(it2);
			}
			it2 = frequencies.find(num_p2);
			if (it2 == frequencies.end()) {
				set<string,ComparacioParaules> s1;
				s1.insert(p2);
				frequencies.insert(make_pair(num_p2, s1));
			} else {
				it2->second.insert(p2);
			}
		} else {
			it1 = frequencies.find(num_p1);
			if (it1 == frequencies.end()) {
				set<string,ComparacioParaules> s1;
				s1.insert(p2);
				frequencies.insert(make_pair(num_p1, s1));
			} else {
				it1->second.insert(p2);
			}
		}
		for (int i = 0; i < frases.size(); ++i) {
			frases[i].substituir(p1, p2);
		}
	}
}

void Text::afegeix_cita(const pair<string,int> &p)
{
	set<pair<string,int> >::iterator it = cites.find(p);
	if (it == cites.end())
		cites.insert(p);
}

void Text::eliminar_cita(const pair<string,int> &p)
{
	set<pair<string,int> >::iterator it = cites.find(p);
	if (it != cites.end())
		cites.erase(it);
}

int Text::nombre_paraules() const
{
	return num_paraules;
}

int Text::nombre_frases() const
{
	return frases.size();
}

void Text::taula_frequencies(string &taula) const
{
	taula = "";
	map<int,set<string,ComparacioParaules> >::const_iterator end = frequencies.end();
	bool first = true;
	while(frequencies.begin() != end) {
		--end;
		set<string,ComparacioParaules>::const_iterator beg_s = end->second.begin();
		while(beg_s != end->second.end()) {
			if (first)
				first = false;
			else
				taula += '\n';
			taula += *(beg_s);
			taula += " ";
			taula += to_string(end->first);
			++beg_s;
		}
	}
}



bool Text::hi_son_paraules(const vector<string> &v) const
{
	int trobades = 0;
	map<int,set<string,ComparacioParaules> >::const_iterator it = frequencies.begin();
	vector<bool> existeixen(v.size(), false);
	analitza_paraula(v, existeixen, autor, trobades);
	analitza_paraula(v, existeixen, titol, trobades);
	while(trobades != v.size() and it != frequencies.end()) {
		int i = 0;
		while(i < v.size()) {
			if (not existeixen[i]) {
				existeixen[i] = it->second.find(v[i]) != it->second.end();
				if (existeixen[i]) {
					++trobades;
				}
			}
			++i;
		}
		++it;
	}
	return trobades == v.size();
}

void Text::frases_x_y(int xfrase, int yfrase, string &frases) const
{
    frases = "";
    bool first = true;
	for (int i = xfrase - 1; i < yfrase; ++i) {
        if (first)
        	first = false;
        else
        	frases += '\n';
        string frase;
        this->frases[i].obtindre_frase(frase);
        frases += to_string(i + 1);
        frases += " ";
        frases += frase;
        
	}
}

void Text::frases_x_y_vector(int xfrase, int yfrase, vector<string> &frases) const
{
	frases = vector<string>(yfrase - xfrase + 1);
	for (int i = xfrase - 1; i < yfrase; ++i) {
		this->frases[i].obtindre_frase(frases[i - xfrase + 1]);
	}
}

void Text::refs_text(set<pair<string,int> >::const_iterator &begin,
               set<pair<string,int> >::const_iterator &end) const
{
    begin = cites.begin();
    end = cites.end();
}


bool Text::frases_expr(const string &expr, string &frases) const
{
	Arbre<string> arb;
	bool b = crea_arbre(arb, expr);
	if (b) {
		frases = "";
		bool first = true;
		for (int i = 0; i < this->frases.size(); ++i) {
 			if (avalua_frase_arbre(arb, this->frases[i])) {
 				if (first)
 					first = false;
 				else
 					frases += '\n';

				frases += to_string(i + 1);
				frases += " ";
				string frase;
				this->frases[i].obtindre_frase(frase);
				frases += frase;
				
			}
		}
	}
	return b;
}

bool Text::avalua_frase_arbre(Arbre<string> &a, const Frase &fr)
{
	bool b;
	string arrel = a.arrel();
	if (arrel != "&" and arrel != "|") {
		b = fr.existeix_paraula(arrel);
	} else {
		Arbre<string> fill1, fill2;
		a.fills(fill1, fill2);
		b = avalua_frase_arbre(fill1, fr);
		if ((not b and arrel == "|") or (b and arrel == "&")) {
			b = avalua_frase_arbre(fill2, fr);
		}
		a.plantar(arrel, fill1, fill2);
	}
	return b;
}

void Text::frases_frase(const string &frase, string &sortida) const
{
	vector<string> v;
	istringstream iss(frase);
	char last = iss.peek();
	while(last != -1) {
		string paraula;
		iss >> paraula;
		v.push_back(paraula);
		ws(iss);
		last = iss.peek();
	}
	bool first = true;
	for (int i = 0; i < frases.size(); ++i) {
		if (frases[i].existeix_paraules_seguides(v)) {
			if (first)
				first = false;
			else
				sortida += "\n";
			sortida += to_string(i + 1);
			sortida += " ";
			string frase;
			frases[i].obtindre_frase(frase);
			sortida += frase;
		}
	}
}
