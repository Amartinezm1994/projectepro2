/** @file Gestor.hh
    @brief Especificació de la classe Gestor
*/
#ifndef GESTOR_HH
#define GESTOR_HH
#include <map>
#include "Arbre.hh"
#include "Autor.hh"
#include "Cita.hh"

/** @class Gestor
    @brief Representa el conjunt de tots els textos dels autors
    y cites associades a ells.
*/
class Gestor {

private:
    /** @brief autors és el diccionari d'autors de tots els textos identificats per
        nom d'autor. */
    map<string,Autor> autors;
    /** @brief cites és el diccionari d'autors de totes les cites associades als textos
        identificades per una referència única. */
    map<string,map<int,Cita> > cites;

    map<string,int> recompte_referencies;

    /** @brief triat_text és el text triat actualment. */
    string triat_text;

    /** @brief triat_autor és l'autor del text triat actualment. */
    string triat_autor;

    /** @brief hi_ha_triat indica si hi ha un text triat. */
    bool hi_ha_triat;

    /** @brief Missatge d'error per defecte. */
    static const string MISSATGE_ERROR;

    /* Invariant:
     *
     *  - autors conté tots els autors amb textos afegits, cada
     *      autor té com a mínim un text o una cita
     *
     *  - cites conté les cites afegides dels textos independentment
     *      de les modificacions als textos.
     *
     *  - Si hi_ha_triat llavors triat_text és el títol d'un text a triat_autor
     *
     *  - les referències a autors i als textos dels autors sempre hi seran a
     *      cites, pero no necessàriament a l'inrevés.
     *
     *  - recompte_referencies conté l'identificador de la última referència afegida
     *      per cada identificador amb lletres generada per les incials 
     *
     *  - recompte_referencies conté l'última referència numérica per cada
     *      coincidència de inicials dels autors que s'han emmagatzemat
     *      al gestor.
     */

    /** @brief Obté les frases des de la x-éssima fins la y-éssima.
        \pre it apunta a un autor vàlid.
        \post sortida conté les frases des de la x-éssima fins la y-éssima numerades.
    */
    bool obtindre_frases(int x, int y, string &sortida, 
                    const map<string,Autor>::const_iterator &it) const;

    /** @brief Obté la informació de la cita apuntada per un iterador.
        \pre it apunta a una cita vàlida, info ha d'estar inicialitzat amb qualsevol cadena de caràcters.
        \post AFEGEIX a info conté la informació de la cita apuntada per it:
            - referència de la cita
            - el número de frase en el text seguit de la frase per totes les frases de la cita 
            - autor seguit del títol entre cometes
    */
    void afegeix_info_cita(bool autor, string &info, const map<int,Cita>::const_iterator &it, const string &ref_name) const;

public:
    /** @brief Constructor per defecte.
        \pre <em>cert</em>
        \post inicialitza els p.i.
    */
    Gestor();

    /** @brief Destructora.
        \pre <em>cert</em>
        \post no treballem amb memòria dinàmica, així que no fa res.
    */
    ~Gestor();


    // OPERACIONS D'ESCRIPTURA (modifiquen el p.i.)

    /** @brief Afegeix un text al gestor.
        \pre <em>cert</em>
        \post es crea un nou text amb autor i titol com identificadors, i text com a contingut,
            en cas que l'autor i el títol ja existeixin, no s'emmagatzema res i sortida conté un 
            missatge d'error.
    */
    void afegir_text(const string &autor, const string &titol,
                     string &text, string &sortida);

    /** @brief Elimina l'últim text seleccionat.
        \pre <em>cert</em>
        \post elimina el text seleccionat, i deixa d'haver-hi un text seleccionat, si no
            hi ha text seleccionat sortida contindrà missatge d'error.
    */
    void eliminar_text(string &sortida);

    /** @brief Substitueix totes les aparicions de la paraula especificada per l'altre paraula especificada.
        \pre <em>cert</em>
        \post substitueïx totes les aparicions de p1 per p2 de l'últim text triat, si
            no hi ha text triat sortida contindrà missatge d'error.
    */
    void substitueix(const string &p1, const string &p2, string &sortida);

    /** @brief Afegeix una cita al conjunt de cites.
        \pre <em>cert</em>
        \post afegeix una cita al sistema basada en les frases que van des de la línia
            x a la línia y del contingut de l'últim text triat, en cas que no es compleixi
            1 <= x <= y <= n, on n és el nombre de frases del text triat, o bé que hi hagi
            una cita en el mateix text amb el mateix interval llavors sortida contindrà un 
            missatge d'error.
    */
    void afegir_cita(int x, int y, string &sortida);

    /** @brief Elimina una cita del conjunt de cites.
        \pre <em>cert</em>
        \post elimina la cita amb referència ref emmagatzemada al gestor, si no existeix
            aquesta referència, sortida contindrà un missatge d'error.
    */
    void eliminar_cita(const string &ref, string &sortida);
    
    /** @brief Tria un text que contingui les paraules especificades.
        \pre <em>cert</em>
        \post es selecciona l'únic text que contingui les paraules pasades per paràmetre,
            si no hi ha cap amb aquestes paraules o hi han dos textos amb aquestes paraules
            sortida contindrà un missatge d'error.
    */
    void triar_text(const string &paraules, string &sortida);


    // OPERACIONS DE CONSULTA (no modifiquen el p.i.)


    /** @brief Mostra tots els textos de l'autor especificat.
        \pre <em>cert</em>
        \post deixa a sortida tots els textos de d'aquest autor ordenats
            alfabeticament, si en té.
    */
    void textos_autor(const string &autor, string &sortida) const;

    /** @brief Mostra tots els autors amb tots els seus textos.
        \pre <em>cert</em>
        \post deixa a sortida tots els autors amb tots els seus textos
            ordenats alfabeticament primer per autor i després per títol. 
            Si no hi han textos sortida és buit.
    */
    void tots_textos(string &sortida) const;

    /** @brief Mostra tots els autors amb les dades de cada autor sobre cada text.
        \pre <em>cert</em>
        \post deixa a sortida tots els autors seguit de el nombre de
           textos total d'aquest autor, el nombre de frases de tots els textos d'aquest autor
            i el nombre de paraules de tots els textos d'aquest autor. Si no hi han textos
            sortida és buit.
    */
    void tots_autors(string &sortida) const;

    /** @brief Mostra tota la informació de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté la informació de l'últim text triat de manera:
            autor, títol, nombre de frases, nombre de paraules i cites associades. Altrament,
            si no hi ha text seleccionat llavors sortida conté un missatge d'error.
    */
    void info(string &sortida) const;

    /** @brief Mostra l'autor de l'últim text triat.
        \pre <em>cert</em>
        \post deixa a sortida l'autor de l'últim text triat, altrament, si no hi ha text
            triat llavors sortida conté un missatge d'error.
    */
    void autor(string &sortida) const;

    /** @brief Mostra el contingut de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté el contingut de l'últim text triat amb totes les seves
            frases numerades, s'escriurà número de frase seguit de la frase. Si no n'hi ha text
            triat llavors sortida conté un missatge d'error.
    */
    void contingut(string &sortida) const;

    /** @brief Mostra les frases des de la x-éssima fins la y-éssima de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté les frases des de la x-èsima fins a la y-èsima
            del text triat numerades, en cas de que no es compleixi 1 <= x <= y <= n, 
            on n és el nombre de frases del text triat o bé no hi hagi text triat, llavors 
            sortida conté un missatge d'error.
    */
    void frases(int x, int y, string &sortida) const;

    /** @brief Mostra el nombre de frases de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté el nombre de frases de l'últim text triat, si no hi ha
            text triat llavors sortida conté un missatge d'error.
    */
    void nombre_frases(string &sortida) const;

    /** @brief Mosotra el nombre de paraules de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté el nombre de paraules de l'últim text triat, si no hi ha
            text triat llavors sortida conté un error.
    */
    void nombre_paraules(string &sortida) const;

    /** @brief Mostra una taula de freqüències en ordre decreixent per freqüència de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté totes les paraules del contingut de l'últim text triat en ordre
            decreixent per freqüència i en cas d'empat les paraules s'ordenen creixentment, 
            primer per llargada i després alfabèticament. Si no hi ha text triat, llavors sortida 
            conté un error.
    */
    void taula_frequencies(string &sortida) const;

    /** @brief Mostra les frases que contenen expr evaluada de l'últim text triat.
        \pre expr tancada per parèntesis i la expressió a evaluar, exemple: expr="(paraula)"
        \post sortida conté les frases del contingut de l'últim text triat avaluades (que
            compleixen l'expressió), si no hi ha text triat llavors sortida conté un missatge 
            d'error.
    */
    void frases_expr(const string &expr, string &sortida) const;

    /** @brief Mostra totes les frases que contenen fr de l'últim text triat.
        \pre fr és la frase a buscar en les frases, per exemple: fr="frase fr"
        \post deixa a sortida les frases numerades que contenen fr íntegrament de l'últim text triat,
            si no hi ha text triat, llavors sortida conté un missatge d'error.
    */
    void frases_frase(const string &fr, string &sortida) const;

    /** @brief Mostra la informació de la cita referènciada.
        \pre <em>cert</em>
        \post sortida conté la informació de la cita referènciada en l'ordre següent: autor,
            títol, num. frase inicial i num. frase final i contingut de la frase o frases que la
            componen. Si no existeix la referència, llavors sortida conté un missatge d'error.
    */
    void info_cita(const string &ref, string &sortida) const;

    /** @brief Mostra totes les cites d'un autor.
        \pre <em>cert</em>
        \post sortida conté totes les cites de l'autor autor com a argument, ordenades per
            referència de manera que: referència, contingut de les frases, autor i text d'on
            provenen. Si no existeix l'autor o bé, l'autor no té cites, llavors sortida és
            buit.
    */
    void cites_autor(const string &autor, string &sortida) const;

    /** @brief Mostra totes les cites de l'últim text triat.
        \pre <em>cert</em>
        \post sortida conté totes les cites de l'últim text triat, d'on se'n mostra: referència,
            contingut de les frases, autor i títol del text d'on provenen. Si no hi ha cap text
            seleccionat, llavors sortida conté un missatge d'error.
    */
    void cites_text(string &sortida) const;

    /** @brief Mostra totes les cites emmagatzemades.
        \pre <em>cert</em>
        \post sortida conté tota la informació de les totes les cites emmagatzemades, d'on
            se'n mostra: la seva referència, contingut de les frases, autor i títol del text
            d'on provenen, ordenades per referència. Si no hi han cites emmagatzemades, llavors
            sortida és buit.
    */
    void totes_cites(string &sortida) const;

};
#endif
