#include <list>
#include <string>
#include <vector>

#include "Frase.hh"
#include "AuxiliarsGenerals.hh"

using namespace std;

Frase::Frase() 
{
	paraules = list<string>();
    existeixen_paraules = false;
}
Frase::~Frase() {}

void Frase::afegir_paraula(const string &p) 
{
	paraules.push_back(p);
    existeixen_paraules = true;
}

void Frase::substituir(const string &p1, const string &p2) 
{
	list<string>::iterator it = paraules.begin();
	while(it != paraules.end()) {
		if (*(it) == p1) {
			it = paraules.erase(it); 
			paraules.insert(it, p2);
		} else {
			++it;
		}
	}
}

void Frase::obtindre_frase(string &str) const 
{
	str = "";
	for (list<string>::const_iterator it = paraules.begin();
			it != paraules.end(); ++it) 
	{
		str += *(it);
	}
}

void Frase::elimina_espai_inicial() 
{
	list<string>::iterator it = paraules.begin();
	if (it != paraules.end() and *(it) == " ") {
		it = paraules.erase(it);
	}
    existeixen_paraules = paraules.begin() != paraules.end();
}

bool Frase::hi_ha_paraules() const 
{
    return existeixen_paraules;
}

bool Frase::existeix_paraula(const string &p) const 
{
	list<string>::const_iterator it = paraules.begin();
	bool trobat = false;
	while(not trobat and it != paraules.end()) {
		trobat = *(it) == p;
		++it;
	}
	return trobat;
}

bool Frase::existeix_paraules_seguides(const vector<string> &v) const 
{
	list<string>::const_iterator it = paraules.begin();
	bool trobat = false;
	while (not trobat and it != paraules.end()) {
		int i = 0;
        if (es_inicial_paraula(*(it)) and *(it) == v[i]) {
			list<string>::const_iterator it2 = it;
			while (i < v.size() and it2 != paraules.end() 
				and *(it2) == v[i])
			{
				++it2;
                while (it2 != paraules.end() and not es_inicial_paraula(*(it2))) {
					++it2;
				}
				++i;
			}
			trobat = i == v.size();
		}
		++it;
	}
	return trobat;
}
