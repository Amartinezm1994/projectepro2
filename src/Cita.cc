#include <vector>
#include <string>

#include "Cita.hh"

using namespace std;

Cita::Cita(const string &autor, const string &titol,
		int xfrase, vector<string> &frases) 
{
	this->frases = frases;
	this->autor = autor;
	this->titol = titol;
	xessima = xfrase;
}

Cita::~Cita() {}

void Cita::autor_cita(string &autor) const 
{
	autor = this->autor;
}

void Cita::titol_cita(string &titol) const 
{
	titol = this->titol;
}

void Cita::frases_cita(vector<string> &frases) const 
{
	frases = vector<string>(this->frases.size());
	for (int i = 0; i < frases.size(); ++i) {
		frases[i] = this->frases[i];
	}
}

void Cita::frases_cita_str(string &sortida) const 
{
	sortida = "";
	bool first = true;
	for (int i = 0; i < frases.size(); ++i) {
		if (first)
			first = false;
		else
			sortida += "\n";
		sortida += to_string(i + xessima);
		sortida += " ";
		sortida += frases[i];
	}
}

int Cita::xfrase() const 
{
	return xessima;
}

int Cita::yfrase() const 
{
	return xessima + frases.size() - 1;
}