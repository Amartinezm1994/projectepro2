#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <set>
#include <cmath>

#include "AuxiliarsGenerals.hh"

using namespace std;

bool es_alfanumeric(char c)
{
    return (c >= 48 and c <= 57) or
           (c >= 65 and c <= 90) or
           (c >= 97 and c <= 122);
}

bool es_puntuacio(char c)
{
    return c == ',' or c == ':' or c == ';' or
           c == '.' or c == '!' or c == '?';
}

bool es_inicial_paraula(const string &p)
{
    return p.size() > 0 and es_alfanumeric(p[0]);
}


void analitza_paraula(const vector<string> &v, vector<bool> &exist,
    const string &cadena, int &trobades)
{
    istringstream iss(cadena);
    char last = iss.peek();
    string par;
    while(trobades != v.size() and last != -1) {
        iss >> par;
        int i = 0;
        while (i < v.size()) {
            if (not exist[i]) {
                char parlast = par[par.size() - 1];
                if (es_puntuacio(parlast))
                    exist[i] = par.substr(0, par.size() - 1) == v[i];
                else {
                    exist[i] = par == v[i];
                }
                if (exist[i])
                    ++trobades;
            }
            ++i;
        }
        ws(iss);
        last = iss.peek();
    }
}

void treu_espais(const string &str, int &i)
{
    while(i < str.size() and (str[i] == ' ' or str[i] == '\t'))
        ++i;
}

bool crea_arbre_nodes(Arbre<string> &a, const string &str, int &i)
{
    treu_espais(str, i);
    bool valid;
    if (i >= str.size())
        valid = false;
    else if (str[i] == '}')
        valid = false;
    else {
        string node = "";
        valid = es_alfanumeric(str[i]);
        while(valid) {
            node += str[i];
            ++i;
            valid = i < str.size() and es_alfanumeric(str[i]);
        }
        treu_espais(str, i);
        valid = i < str.size();
        if (valid) {
            if (str[i] == '}') {
                Arbre<string> fill1, fill2;
                a.plantar(node, fill1, fill2);
            } else if (es_alfanumeric(str[i])) {
                Arbre<string> fill1, fill2, filla1, filla2;
                valid = crea_arbre_nodes(fill2, str, i);
                if (valid) {
                    fill1.plantar(node, filla1, filla2);
                    a.plantar("&", fill1, fill2);
                }
            } else
                valid = false;
        }
    }
    return valid;
}

bool i_crea_arbre(Arbre<string> &a, const string &str, int &i)
{
    bool valid;
    if (i >= str.size())
        valid = false;
    else if (str[i] == ')')
        valid = false;
    else {
        treu_espais(str, i);
        valid = i < str.size();
        if (valid) {
            Arbre<string> fill1, fill2;
            if (str[i] == '(') {
                ++i;
                valid = i_crea_arbre(fill1, str, i);
            } else if (str[i] == '{') {
                ++i;
                valid = crea_arbre_nodes(fill1, str, i);
            } else
                valid = false;
            if (valid) {
                ++i;
                treu_espais(str, i);
                valid = i < str.size();
                if (valid and (str[i] == '&' or str[i] == '|')) {
                    string node = "";
                    node += str[i];
                    ++i;
                    treu_espais(str, i);
                    valid = i < str.size();
                    if (valid and str[i] == '(') {
                        ++i;
                        valid = i_crea_arbre(fill2, str, i);
                    } else if (valid and str[i] == '{') {
                        ++i;
                        valid = crea_arbre_nodes(fill2, str, i);
                    } else
                        valid = false;
                    if (valid) {
                        ++i;
                        treu_espais(str, i);
                        valid = i < str.size() and str[i] == ')';
                        if (valid) {
                            a.plantar(node, fill1, fill2);
                        }
                    }
                } else if (valid and (str[i] == ')')) {
                    a = fill1;
                } else
                    valid = false;
            }
        }
    }
    return valid;
}

bool crea_arbre(Arbre<string> &a, const string &str)
{
    int i = 1;
    bool valid = i < str.size();
    if (str[0] == '{') {
        valid = crea_arbre_nodes(a, str, i);
    } else if (str[0] == '(') {
        valid = i_crea_arbre(a, str, i);
    } else
        valid = false;
    return valid;
}

void imprimeix_arbre(Arbre<string> &a)
{
    if (not a.es_buit()) {
        string arrel = a.arrel();
        Arbre<string> f1, f2;
        a.fills(f1, f2);
        imprimeix_arbre(f1);
        cout << arrel;
        imprimeix_arbre(f2);
        a.plantar(arrel, f1, f2);
    }
}

void desglosa_referencia(const string &str, string &ref, int &num)
{
    int i = str.size();
    bool es_num = true;
    while (es_num and i >= 0) {
        --i;
        es_num = str[i] >= '0' and str[i] <= '9';
    }
    ref = str.substr(0, i + 1);
    num = 0;
    ++i;
    int j = str.size() - i;
    while (i < str.size()) {
        --j;
        num += pow(10, j)*int(str[i] - '0');
        ++i;
    }
}

void normalitza_cadena(const string &str, string &nou)
{
    nou = "";
    istringstream iss(str);
    char last = iss.peek();
    bool first = true;
    while(last != -1) {
        if (first)
            first = false;
        else
            nou += " ";
        string paraula;
        iss >> paraula;
        nou += paraula;
        ws(iss);
        last = iss.peek();
    }
}

void normalitza_titol(const string &str, string &nou)
{
    nou = "";
    istringstream iss(str);
    char last = iss.peek();
    bool first = true;
    while(last != -1) {
        if (last != '.' and last != '?' and last != '!' and
            last != ';' and last != ':' and last != ',') {
            if (first)
                first = false;
            else
                nou += " ";
        }
        string paraula;
        iss >> paraula;
        nou += paraula;
        ws(iss);
        last = iss.peek();
    }
}

void obtindre_conjunt_cadena(const string &str, set<string> &s)
{
    s = set<string>();
    istringstream iss(str);
    char last = iss.peek();
    while(last != -1) {
        string paraula;
        iss >> paraula;
        s.insert(paraula);
        ws(iss);
        last = iss.peek();
    }
}

bool son_tot_paraules(const string &str)
{
    int i = 0;
    bool tot_paraules = true;
    while (tot_paraules and i < str.size()) {
        tot_paraules = es_alfanumeric(str[i]) or str[i] == ' ' or str[i] == '\t';
        ++i;
    }
    return tot_paraules;
}

bool es_unica_paraula(const string &str)
{
    int i = 0;
    bool una_paraula = true;
    while (una_paraula and i < str.size()) {
        una_paraula = str[i] == ' ' or str[i] == '\t';
        ++i;
    }
    una_paraula = i < str.size() and es_alfanumeric(str[i]);
    while (una_paraula and i < str.size()) {
        una_paraula = es_alfanumeric(str[i]);
        ++i;
    }
    una_paraula = i == str.size() or (str[i] == ' ' or str[i] == '\t');
    while (una_paraula and i < str.size()) {
        una_paraula = str[i] == ' ' or str[i] == '\t';
        ++i;
    }
    return una_paraula;
}
