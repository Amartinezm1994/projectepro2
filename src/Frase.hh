/** @file Frase.hh
    @brief Especificació de la classe Frase
*/
#ifndef FRASE_HH
#define FRASE_HH
#include <list>
#include <string>
#include <vector>


using namespace std;

/** @class Frase
    @brief Representa una frase que conté paraules.
*/
class Frase {
private:
	/** @brief Conté totes les paraules de representació. */
	list<string> paraules;

	/** @brief Indica si existeixen paraules a la representació. */
    bool existeixen_paraules;

	/* Invariant:
		- paraules conté totes les paraules, signes de puntuació
			i espais per separat.
		- si existeixen_paraules llavors paraules conté una o més paraules,
			altrament, si existeixen_paraules és fals paraules és buida.
	*/


public:
	/** @brief Constructor per defecte.
		\pre <em>cert</em>
		\post inicialitza el p.i.
	*/
	Frase();
	~Frase();

	// OPERACIONS D'ESCRIPTURA (modifiquen el p.i.)

	/** @brief Afegeix una paraula a la frase.
		\pre <em>cert</em>
		\post la paraula ha sigut afegida al final de la frase del p.i.
		\cost Lineal respecte mida de p, constant respecte mida de paraules
	*/
	void afegir_paraula(const string &p);

	/** @brief Substitueix totes les aparicions de p1 per p2.
        \pre hi han paraules a la frase.
        \post totes les aparicions de p1 han sigut substituïdes per p2.
        \cost Estrictament lineal respecte els elements de paraules.
	*/
	void substituir(const string &p1, const string &p2);

	/** @brief Elimina el posible espai inicial de la paraula.
        \pre hi han paraules a la frase.
		\post si la primera paraula és un espai, aquest s'elimina.
		\cost Constant
	*/
	void elimina_espai_inicial();
	

	// OPERACIONS DE CONSULTA (no modiquen el p.i.)

	/** @brief Dóna la frase del p.i.
        \pre hi han paraules a la frase.
		\post str és la frase del p.i.
		\cost Estrictament lineal
	*/
	void obtindre_frase(string &str) const;

    /** @brief Diu si hi han paraules a la frase
        \pre <em>cert</em>
        \post retorna verdader si hi ha alguna paraula a la frase
            altrament fals.
    */
    bool hi_ha_paraules() const;

	/** @brief Diu si existeix p en la frase.
		\pre hi han paraules a la frase.
		\post retorna verdader si existeix la paraula, altrament fals.
	*/
	bool existeix_paraula(const string &p) const;

    /** @brief Diu si les paraules del vector v estàn consecutivament com
     *     a paraules vàlides a la frase.
     *  \pre <em>cert</em>
     *  \post si el retorn és cert, llavors les paraules de v es troben
     *      consecutivament a la frase, altrament si el retorn és fals
     *      llavors les paraules de v no es troben consecutivament a la
     *      frase.
    */
	bool existeix_paraules_seguides(const vector<string> &v) const;

};
#endif
