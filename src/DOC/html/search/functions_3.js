var searchData=
[
  ['elimina_5fespai_5finicial',['elimina_espai_inicial',['../class_frase.html#a86896a53060a4900031b413763279726',1,'Frase']]],
  ['eliminar_5fcita',['eliminar_cita',['../class_autor.html#af5a73106085c23f99f4329dcb42bb856',1,'Autor::eliminar_cita()'],['../class_gestor.html#a7ba4ce75f829cb7183fbf45a6775e49c',1,'Gestor::eliminar_cita()'],['../class_text.html#a6251bba534b77188c85ccabc5125cecf',1,'Text::eliminar_cita()']]],
  ['eliminar_5ftext',['eliminar_text',['../class_autor.html#a1f0ebb8259d2f67c71ebf4d39dc9f1cd',1,'Autor::eliminar_text()'],['../class_gestor.html#a464f73cac29bb22340546abcada70aa0',1,'Gestor::eliminar_text()']]],
  ['es_5falfanumeric',['es_alfanumeric',['../_auxiliars_generals_8cc.html#a16d7aff2398d3f4a8582110b1978680f',1,'es_alfanumeric(char c):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a16d7aff2398d3f4a8582110b1978680f',1,'es_alfanumeric(char c):&#160;AuxiliarsGenerals.cc']]],
  ['es_5finicial_5fparaula',['es_inicial_paraula',['../_auxiliars_generals_8cc.html#a4fecd7387c29c090e65a69ed70622e63',1,'es_inicial_paraula(const string &amp;p):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a4fecd7387c29c090e65a69ed70622e63',1,'es_inicial_paraula(const string &amp;p):&#160;AuxiliarsGenerals.cc']]],
  ['es_5fpuntuacio',['es_puntuacio',['../_auxiliars_generals_8cc.html#afec0669af29d5b11815433e1234edc79',1,'es_puntuacio(char c):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#afec0669af29d5b11815433e1234edc79',1,'es_puntuacio(char c):&#160;AuxiliarsGenerals.cc']]],
  ['es_5funica_5fparaula',['es_unica_paraula',['../_auxiliars_generals_8cc.html#a64fd4c819350c5a2d312f9a11e3d537b',1,'es_unica_paraula(const string &amp;str):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a64fd4c819350c5a2d312f9a11e3d537b',1,'es_unica_paraula(const string &amp;str):&#160;AuxiliarsGenerals.cc']]],
  ['existeix_5fcita',['existeix_cita',['../class_autor.html#a3d45ed36efade0e2d8c305358e356173',1,'Autor']]],
  ['existeix_5fparaula',['existeix_paraula',['../class_frase.html#ade0b1fef4e67ac01ea2a0101615bdca3',1,'Frase']]],
  ['existeix_5fparaules_5fseguides',['existeix_paraules_seguides',['../class_frase.html#ab67e9c0a15ce777ef28b11e8c8cfe44d',1,'Frase']]]
];
