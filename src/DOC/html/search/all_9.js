var searchData=
[
  ['nom',['nom',['../class_autor.html#ac8ec05c00840d794bae61edf1e85dc6f',1,'Autor']]],
  ['nombre_5ffrases',['nombre_frases',['../class_autor.html#a2e9ba60f09ed81feef728ad875d2a77c',1,'Autor::nombre_frases()'],['../class_gestor.html#add8fe540ef02887df473d1b2c95a1872',1,'Gestor::nombre_frases()'],['../class_text.html#a5f7b7338cf217a09cf9045ca819e6563',1,'Text::nombre_frases()']]],
  ['nombre_5fparaules',['nombre_paraules',['../class_autor.html#a40e3a818356d751b6eca44219cbe9d74',1,'Autor::nombre_paraules()'],['../class_gestor.html#a101b5d68675f8d9ae84b9067325bf134',1,'Gestor::nombre_paraules()'],['../class_text.html#ace001f96970770bf74fec2281d5230b2',1,'Text::nombre_paraules()']]],
  ['nombre_5ftextos',['nombre_textos',['../class_autor.html#a90d506a2e47b978b6eaec1dd8974f27a',1,'Autor']]],
  ['normalitza_5fcadena',['normalitza_cadena',['../_auxiliars_generals_8cc.html#a7d3a4ca2097a0f94b1b28bf81ac8a148',1,'normalitza_cadena(const string &amp;str, string &amp;nou):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a7d3a4ca2097a0f94b1b28bf81ac8a148',1,'normalitza_cadena(const string &amp;str, string &amp;nou):&#160;AuxiliarsGenerals.cc']]],
  ['normalitza_5ftitol',['normalitza_titol',['../_auxiliars_generals_8cc.html#ae877eb511a80690b6c30fff888b04652',1,'normalitza_titol(const string &amp;str, string &amp;nou):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#ae877eb511a80690b6c30fff888b04652',1,'normalitza_titol(const string &amp;str, string &amp;nou):&#160;AuxiliarsGenerals.cc']]],
  ['num_5ffrases',['num_frases',['../class_autor.html#ae1f3d221d84a859642af248fa4c06dc7',1,'Autor']]],
  ['num_5ffrases_5ftriat',['num_frases_triat',['../class_autor.html#a8c6fdcdf125c09dd3ca9d08b8d7b4af4',1,'Autor']]],
  ['num_5fparaules',['num_paraules',['../class_autor.html#a71ed1dc626d31607b5a4655da66db48b',1,'Autor::num_paraules()'],['../class_text.html#a767fa4f48f28e4b91ed8c7dfc0395ce9',1,'Text::num_paraules()']]],
  ['num_5fparaules_5ftriat',['num_paraules_triat',['../class_autor.html#aab602d2a751bea129ea2e757a48a00e1',1,'Autor']]]
];
