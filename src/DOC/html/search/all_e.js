var searchData=
[
  ['taula_5ffrequencies',['taula_frequencies',['../class_autor.html#adf67c26111e14af95193ec6a4cecd1f1',1,'Autor::taula_frequencies()'],['../class_gestor.html#a2423169e9da38f7ca34e3aee1e7cb123',1,'Gestor::taula_frequencies()'],['../class_text.html#aac4e45b5443cc73cc58fe5c7af9fbf6a',1,'Text::taula_frequencies()']]],
  ['text',['Text',['../class_text.html',1,'Text'],['../class_text.html#a0115c527bc2fe8db0242d5dd41d4143e',1,'Text::Text()']]],
  ['text_2ecc',['Text.cc',['../_text_8cc.html',1,'']]],
  ['text_2ehh',['Text.hh',['../_text_8hh.html',1,'']]],
  ['textos',['textos',['../class_autor.html#ad09934c90e512e332a362a20f572b2a0',1,'Autor']]],
  ['textos_5fautor',['textos_autor',['../class_gestor.html#a5a29d5510cccb0b0becdce60497d42b7',1,'Gestor']]],
  ['titol',['titol',['../class_cita.html#ae48282a8dd494783a60af6da42a057e5',1,'Cita::titol()'],['../class_text.html#abebeee870d4c29258cd1641b0e5f0947',1,'Text::titol()']]],
  ['titol_5fcita',['titol_cita',['../class_cita.html#a561388d1ab13e66529f64457e0f1e598',1,'Cita']]],
  ['titols_5ftextos',['titols_textos',['../class_autor.html#a1686b9f1a1725de2dd3f26e662ba4fed',1,'Autor']]],
  ['totes_5fcites',['totes_cites',['../class_gestor.html#aca060dbf68c6cfd8d85b5b17d17ae121',1,'Gestor']]],
  ['tots_5fautors',['tots_autors',['../class_gestor.html#a2e4c6d99c883f16e12ae1230c47ecfcd',1,'Gestor']]],
  ['tots_5ftextos',['tots_textos',['../class_gestor.html#abb61538b0821439c17dad988abe02631',1,'Gestor']]],
  ['treu_5fespais',['treu_espais',['../_auxiliars_generals_8cc.html#ab0b9280a8b7633290c99dec191572de7',1,'treu_espais(const string &amp;str, int &amp;i):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#ab0b9280a8b7633290c99dec191572de7',1,'treu_espais(const string &amp;str, int &amp;i):&#160;AuxiliarsGenerals.cc']]],
  ['triar_5ftext',['triar_text',['../class_gestor.html#a23dc738f0d7b12cdd0dc044ffc5431ea',1,'Gestor']]],
  ['triat_5fautor',['triat_autor',['../class_gestor.html#ac71edd315f0ca2c0dec40ff6e9244586',1,'Gestor']]],
  ['triat_5ftext',['triat_text',['../class_gestor.html#a9c8449ca00db9418fb01ef5a68433db5',1,'Gestor']]]
];
