var searchData=
[
  ['afegeix_5fcita',['afegeix_cita',['../class_autor.html#ae11e62591c444fb3b269a726f1146df5',1,'Autor::afegeix_cita()'],['../class_text.html#a1bad303f4e712e37fae85b0fa6d2b7db',1,'Text::afegeix_cita()']]],
  ['afegeix_5finfo_5fcita',['afegeix_info_cita',['../class_gestor.html#a403fe24ac13a8dfc945821ec5c499199',1,'Gestor']]],
  ['afegir_5fcita',['afegir_cita',['../class_gestor.html#afcec0929c8ae0a473d5571cf928267de',1,'Gestor']]],
  ['afegir_5fparaula',['afegir_paraula',['../class_frase.html#a3f279110a56913ccf007ba1a0edab38d',1,'Frase']]],
  ['afegir_5fparaula_5ftaula_5ffrequencies',['afegir_paraula_taula_frequencies',['../class_text.html#aa060ea7aa7e4bae003e9bc51a2023d52',1,'Text']]],
  ['afegir_5ftext',['afegir_text',['../class_autor.html#a32226d9800388d10841886452c1036fb',1,'Autor::afegir_text()'],['../class_gestor.html#ad94be58a4cf64694d04ed9270a77d42f',1,'Gestor::afegir_text()']]],
  ['analitza_5fparaula',['analitza_paraula',['../_auxiliars_generals_8cc.html#a8ea52d62b58ab41b4a477992492185b6',1,'analitza_paraula(const vector&lt; string &gt; &amp;v, vector&lt; bool &gt; &amp;exist, const string &amp;cadena, int &amp;trobades):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a8ea52d62b58ab41b4a477992492185b6',1,'analitza_paraula(const vector&lt; string &gt; &amp;v, vector&lt; bool &gt; &amp;exist, const string &amp;cadena, int &amp;trobades):&#160;AuxiliarsGenerals.cc']]],
  ['autor',['Autor',['../class_autor.html',1,'Autor'],['../class_autor.html#a44a51a82ebbe68ce9e57f5a1ff1f2ca7',1,'Autor::Autor()'],['../class_cita.html#a30f543c72768544f9dab335ea0c39b34',1,'Cita::autor()'],['../class_text.html#ad8f978ad7d2735c371990cce7bdc0d8f',1,'Text::autor()'],['../class_gestor.html#a2cb28d6c73cfadcc0ce46fc466e93fab',1,'Gestor::autor()']]],
  ['autor_2ecc',['Autor.cc',['../_autor_8cc.html',1,'']]],
  ['autor_2ehh',['Autor.hh',['../_autor_8hh.html',1,'']]],
  ['autor_5fcita',['autor_cita',['../class_cita.html#a2332c97738ed74d772dbaeae27503a4e',1,'Cita']]],
  ['autor_5ftitols_5ftextos',['autor_titols_textos',['../class_autor.html#a48c6535e15be171f2fc6d6f63ba3db95',1,'Autor']]],
  ['autors',['autors',['../class_gestor.html#ace3e7d37e9740cc676ecc17940be6fed',1,'Gestor']]],
  ['auxiliarsgenerals_2ecc',['AuxiliarsGenerals.cc',['../_auxiliars_generals_8cc.html',1,'']]],
  ['auxiliarsgenerals_2ehh',['AuxiliarsGenerals.hh',['../_auxiliars_generals_8hh.html',1,'']]],
  ['avalua_5ffrase_5farbre',['avalua_frase_arbre',['../class_text.html#a0f66b0ebe878cdbac6b846be0162ad3d',1,'Text']]]
];
