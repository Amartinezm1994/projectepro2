var searchData=
[
  ['frase',['Frase',['../class_frase.html#a6af6ccf07cac65950917bc81f5e03c95',1,'Frase']]],
  ['frases',['frases',['../class_autor.html#a3b39015d5dd08bf6c7e74b7e18901046',1,'Autor::frases()'],['../class_gestor.html#a8b0c70cb2806013bf19232fcef34a1e0',1,'Gestor::frases()']]],
  ['frases_5fcita',['frases_cita',['../class_cita.html#a2715e3708bd913cbe249760a9400e032',1,'Cita']]],
  ['frases_5fcita_5fstr',['frases_cita_str',['../class_cita.html#a765652b755eff82076e166f930b1216c',1,'Cita']]],
  ['frases_5fexpr',['frases_expr',['../class_autor.html#ab9380652730750b1cbd9f6e471b47ce1',1,'Autor::frases_expr()'],['../class_gestor.html#ab04197f19632dde42b5b2ffae4d02571',1,'Gestor::frases_expr()'],['../class_text.html#aa289e1e6c7e319af61c41c4da2241b7a',1,'Text::frases_expr()']]],
  ['frases_5ffrase',['frases_frase',['../class_autor.html#ad7f736c83106cd34d09edf1130cf4eb2',1,'Autor::frases_frase()'],['../class_gestor.html#aeb4fc2ae960f69bb43639c1634a5dde3',1,'Gestor::frases_frase()'],['../class_text.html#a807fb9bd1a76eae54582f830590d92bd',1,'Text::frases_frase()']]],
  ['frases_5fit_5ftext',['frases_it_text',['../class_autor.html#affc294ef501ca92a1d772f26e3ff4a8f',1,'Autor']]],
  ['frases_5frang_5fcorrecte',['frases_rang_correcte',['../class_autor.html#acbec789598fb9c6bbc3bedd81e993e12',1,'Autor']]],
  ['frases_5fvector',['frases_vector',['../class_autor.html#ae472b4faafc55da5d5d3d975d0928f41',1,'Autor']]],
  ['frases_5fx_5fy',['frases_x_y',['../class_text.html#ab0c17408716d397bf6287bc89dd76a22',1,'Text']]],
  ['frases_5fx_5fy_5fvector',['frases_x_y_vector',['../class_text.html#a4f988a299d8556ffe1d8568b2bfab657',1,'Text']]]
];
