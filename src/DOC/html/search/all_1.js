var searchData=
[
  ['cita',['Cita',['../class_cita.html',1,'Cita'],['../class_cita.html#ac7f9cd013663ebde41d6c3e56afe8098',1,'Cita::Cita()']]],
  ['cita_2ecc',['Cita.cc',['../_cita_8cc.html',1,'']]],
  ['cita_2ehh',['Cita.hh',['../_cita_8hh.html',1,'']]],
  ['cites',['cites',['../class_autor.html#a87d286ed5724ef9dcf8047a2212db086',1,'Autor::cites()'],['../class_gestor.html#ad504baaf313da2a19cc745520561dc34',1,'Gestor::cites()'],['../class_text.html#a972b29411506c3f97dc8d3c1e5c49e9b',1,'Text::cites()']]],
  ['cites_5fautor',['cites_autor',['../class_gestor.html#a8f8d76c5fb3698c42013eb9df24a4484',1,'Gestor']]],
  ['cites_5ftext',['cites_text',['../class_gestor.html#a07ec69060d0cd3c3dd4e1b6a713d281e',1,'Gestor']]],
  ['comparacioparaules',['ComparacioParaules',['../struct_text_1_1_comparacio_paraules.html',1,'Text']]],
  ['comprova_5frang_5ffrases',['comprova_rang_frases',['../class_autor.html#a45c895e8dae328ed7514161bc36b535f',1,'Autor']]],
  ['contingut',['contingut',['../class_gestor.html#a1821f7d9973fbedfa70c13310092f75d',1,'Gestor']]],
  ['crea_5farbre',['crea_arbre',['../_auxiliars_generals_8cc.html#a24b4fa96ed23a47142735b7a5b2ef86d',1,'crea_arbre(Arbre&lt; string &gt; &amp;a, const string &amp;str):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a468c5c38a9ab8eb5dfc13c749d6aeb65',1,'crea_arbre(Arbre&lt; string &gt; &amp;a, const string &amp;expr):&#160;AuxiliarsGenerals.cc']]],
  ['crea_5farbre_5fnodes',['crea_arbre_nodes',['../_auxiliars_generals_8cc.html#a9829a279cc71b4cb4de2503c28c37273',1,'crea_arbre_nodes(Arbre&lt; string &gt; &amp;a, const string &amp;str, int &amp;i):&#160;AuxiliarsGenerals.cc'],['../_auxiliars_generals_8hh.html#a9829a279cc71b4cb4de2503c28c37273',1,'crea_arbre_nodes(Arbre&lt; string &gt; &amp;a, const string &amp;str, int &amp;i):&#160;AuxiliarsGenerals.cc']]]
];
