/** @file Autor.hh
    @brief Especificació de la classe Autor
*/
#ifndef AUTOR_HH
#define AUTOR_HH
#include <map>
#include <set>
#include <string>

#include "Text.hh"

/** @class Autor
    @brief Representa un autor el qual té textos escrits per ell.
*/
class Autor {
private:
    /** @brief nom és el nom de l'autor representat. */
    string nom;
    /** @brief textos representa tots els textos d'aquest autor. */
	map<string,Text> textos;
    /** @brief num_frases és el nombre total de frases de tots els textos
            d'aquest autor. */
    int num_frases;
    /** @brief num_paraules és el nombre total de paraules de tots els textos
            d'aquest autor. */
    int num_paraules;
    /** @brief cites són les referències a cites d'aquest autor ordenades alfabéticament. */
    set<pair<string,int> > cites;


    /* Invariant:
     *
     *  - textos conté tots els textos d'aquest autor.
     *
     *  - la representació sempre contindrà almenys una cita o un text.
     *
     *  - num_frases és el nombre de frases total de tots els textos.
     *
     *  - num_paraules és el nombre de paraules total de tots els textos.
     *
     *  - cites són les referències de cites creades d'aquest autor
     *      ordenades alfabéticament.
     *
     *  - nom és el nom d'aquest autor
     *
     * */

    /** @brief Dóna el nombre de frases del text apuntat per un iterador.
        \pre it és un iterador que apunta a un text vàlid.
        \post el retorn és el nombre de frases del text apuntat per it.
    */
    int frases_it_text(const map<string,Text>::const_iterator &it) const;
    
    /** @brief Dóna el nombre de paraules del text apuntat per un iterador.
        \pre it és un iterador que apunta a un text vàlid.
        \post el retorn és el nombre de paraules del text apuntat per it.
    */
    int paraules_it_text(const map<string,Text>::const_iterator &it) const;
    
    /** @brief Dóna l'iterador d'inici i final del conjunt de referències de cites
        del text apuntat per un iterador.
        \pre it és un iterador que apunta a un text vàlid.
        \post begin i end són els iteradors principi i final del set de referències 
            del text al que apunta it.
    */
    void iteradors_refs_it_text(set<pair<string,int> >::const_iterator &begin,
                    set<pair<string,int> >::const_iterator &end, 
                    const map<string,Text>::const_iterator &it) const;

    /** @brief Diu si el rang de frases és correcte per al text triat.
        \pre triat_text és un títol de un text d'aquest autor.
        \post si el retorn és cert, llavors el rang de frases és vàlid 
            i it conté l'iterador d'aquest text, altrament, si el retorn és
            fals el rang no és correcte i it no apunta necessàriament a un
            text vàlid.
    */
    bool comprova_rang_frases(int xfrase, int yfrase, const string &triat_text, 
        map<string,Text>::const_iterator &it) const;

public:

    /** @brief Constructor de la classe autor.
        \pre <em>cert</em>
        \post es crea el primer text amb el nom de l'autor, el títol i el text i
            s'inicialitzen els p.i.
    */
    Autor(const string &autor, const string &titol, string &text);
    ~Autor();


    // OPERACIONS D'ESCRIPTURA (modifiquen el p.i.)

    /** @brief Afegeix un nou text.
        \pre <em>cert</em>
        \post si no existeix un text amb aquest títol s'afegeix el text amb
            amb el títol titol i amb el text text dels arguments i el retorn es
            cert, altrament no s'afegeix res i el retorn és fals.
    */
    bool afegir_text(const string &titol, string &text);

    /** @brief Elimina l'últim text seleccionat.
        \pre triat és el títol d'un text en aquest autor.
        \post elimina l'últim text triat.
    */
    void eliminar_text(const string &triat);

    /** @brief Substitueix totes les aparicions d'una paraula de l'últim text triat
        per altre.
        \pre existeixen textos a l'autor i, p1 i p2 han de ser diferents.
        \post canvia totes les aparicions de p1 per p2 en el contingut de l'últim
            text triat.
    */
    void substitueix(const string &p1, const string &p2, const string &triat);

    /** @brief Afegeix la referència d'una cita a l'autor.
        \pre <em>cert</em>
        \post si no existeix la referència d'aquesta cita s'afegeix la referència de 
            la cita al conjunt de referències de les cites i el retorn és cert, altrament
            el retorn és fals.
    */
    bool afegeix_cita(const pair<string,int> &p, const string &titol_text);

    /** @brief Elimina la referència de la cita especificada d'aquest autor.
        \pre existeixen cites en aquest autor.
        \post si existeix la referència d'aquesta cita llavors s'elimina aquesta de
            les cites d'aquest autor i el retorn és cert, altrament no s'elimina res.
    */
    bool eliminar_cita(const pair<string,int> &p, const string &titol_text);

    /** @brief Selecciona el text amb les paraules indicades.
        \pre existeixen textos a l'autor.
        \post s'aplica l'esquema de cerca, para en el moment en que troba dos
          textos amb les mateixes paraules:
          - si el retorn és -1 llavors no ha trobat el text.
          - si el retorn és 0 llavors hi han dos textos amb les mateixes paraules.
          - si el retorn és 1 llavors s'ha trobat el text i s'ha seleccionat.
    */
    int selecciona_text(const vector<string> &v, string &trobat);


    // OPERACIONS DE CONSULTA (no modifiquen el p.i.)

    /** @brief Comprova si hi han textos en l'autor.
        \pre <em>cert</em>
        \post retorna true si hi ha almenys un text a l'autor, altrament false.
    */
    bool hi_ha_textos() const;

    /** @brief Comprova si hi han referències de cites a l'autor.
        \pre <em>cert</em>
        \post retorna cert si hi ha almenys una cita a l'autor, altrament retorna fals.
    */
    bool hi_han_cites() const;

    /** @brief Comprova si existeix una determinada referència en aquest autor.
        \pre <em>cert</em>
        \post si existeix la referència de la cita a l'autor llavors el valor de retorn és
            cert, altrament és fals.
    */
    bool existeix_cita(const pair<string,int> &ref) const;

    //bool rang_correcte

    /** @brief Diu el nombre de textos d'aquest autor.
        \pre <em>cert</em>
        \post retorna el nombre de textos d'aquest autor.
    */
    int nombre_textos() const;

    /** @brief Diu el nombre de frases de tots els textos d'aquest autor.
        \pre <em>cert</em>
        \post retorna el nombre de frases de tots els textos d'aquest autor.
    */
    int nombre_frases() const;

    /** @brief Diu el nombre de paraules de tots els textos d'aquest autor.
        \pre <em>cert</em>
        \post retorna el nombre de paraules de tots els textos d'aquest autor.
    */
    int nombre_paraules() const;

    /** @brief Diu tots els títols d'aquest autor.
        \pre existeixen textos a l'autor.
        \post titols conté tots els títols dels textos, per cada línia el 
            nom d'autor seguit del títol del text entre cometes.
    */
    void autor_titols_textos(string &titols) const;

    /** @brief Diu tots els títols d'aquest autor.
        \pre existeixen textos a l'autor.
        \post titols conté tots els títols dels textos, per cada línia 
            el títol del text entre cometes.
    */
    void titols_textos(string &titols) const;

    /** @brief Dóna les frases des de la x-éssima fins la y-éssima de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post frases conté les frases des de la x-éssima frase a la y-éssima frase de l'últim
            text triat, si no es compleix 1 <= xfrase <= yfrase <= nfrases
            retorna fals.
    */
    bool frases(int xfrase, int yfrase, string &frases, const string &triat) const;

    /** @brief Dóna les frases des de la x-éssima fins la y-éssima de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post v és el vector de la x-éssima frase a la y-éssima frase de l'últim
            text triat, si no es compleix 1 <= xfrase <= yfrase <= nfrases
            retorna fals.
    */
    bool frases_vector(int xfrase, int yfrase, vector<string> &frases, const string &triat) const;

    /** @brief Diu si xfrase i yfrase són a un rang correcte al text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post si el retorn és cert llavors el rang de frases en el text triat,
            altrament, si el retorn és fals llavors el rang és incorrecte.
    */
    bool frases_rang_correcte(int xfrase, int yfrase, const string &triat);

    /** @brief Diu el número de frases total de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post retorna el nombre de frases de l'últim text triat.
    */
    int num_frases_triat(const string &triat) const;

    /** @brief Diu el número de paraules total de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post retorna el nombre de paraules de l'últim text triat.
    */
    int num_paraules_triat(const string &triat) const;

    /** @brief Dóna la taula de freqüències d'aquest text.
        \pre triat és el títol d'un text en aquest autor.
        \post taula conté la taula de freqüències de l'últim text triat per ordre 
            descendent per freqüència, en cas d'empat s'ordenen creixentment per 
            llargada de paraula i després alfabéticament.
    */
    void taula_frequencies(string &taula, const string &triat) const;

    /** @brief Dóna les frases que contenen l'expressió avaluada  de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post si el retorn és cert, frases conté les frases numerades que compleixen l'expressió
            de l'últim text triat, si el retorn és fals, expr està mal formada. 
    */
    bool frases_expr(const string &expr, string &frases, const string &triat) const;

    /** @brief Dóna les frases que contenen el fragment de text de frase de l'últim text triat.
        \pre triat és el títol d'un text en aquest autor.
        \post v conté totes les frases que contenen íntegrament frase de l'últim
            text triat.
    */
    void frases_frase(const string &frase, string &sortida, const string &triat) const;

    /** @brief Dóna un iterador d'inici i un altre de final de les referències de cites del text seleccionat.
        \pre triat és el títol d'un text en aquest autor.
        \post begin conté l'iterador d'inici i end conté l'iterador del final del set que conté totes
            les referències de cites associades al text amb títol triat.
    */
    void refs_text(set<pair<string,int> >::const_iterator &begin,
                   set<pair<string,int> >::const_iterator &end, const string &triat) const;

    /** @brief Dóna un iterador d'inici i un altre de final de les referències d'aquest autor.
        \pre <em>cert</em>
        \post begin conté l'iterador d'inici i end conté l'iterador del final del set que conté
            totes les referències de cites d'aquest autor.
    */
    void refs_autor(set<pair<string,int> >::const_iterator &begin,
                    set<pair<string,int> >::const_iterator &end) const;

    /** @brief Dóna un iterador d'inici i un altre de final de les referències de cites del text seleccionat,
            el nombre de frases i el nombre de paraules d'aquest.
        \pre triat és el títol d'un text en aquest autor.
        \post begin conté l'iterador d'inici i end conté l'iterador del final del set que conté totes
            les referències de cites associades al text amb títol triat, num_paraules és el nombre de
            paraules d'aquest i num_paraules és el nombre de paraules d'aquest.
    */
    void refs_paraules_frases_text(set<pair<string,int> >::const_iterator &begin,
                   set<pair<string,int> >::const_iterator &end, int &num_frases, 
                   int &num_paraules, const string &triat) const;
};
#endif
