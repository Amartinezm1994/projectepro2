#include <iostream>
#include <sstream>

#include "Gestor.hh"
#include "AuxiliarsGenerals.hh"

using namespace std;



int main()
{
	
    /* Important
     *  - les comandes de consulta han de finalitzar en ?
     * 	- tot error que es tingui que mostrar, s'eva lua dins el gestor
     */	
	string linia;
	string sortida;
    Gestor gestor;

    // A dalt de cada if està indicat quina comanda volem analitzar
	getline(cin,linia);
    // Comença la lectura de comandes.  
	while (linia!="sortir"){

		sortida = "";
		istringstream iss(linia);
		string op;
        iss >> op;
        
        cout << linia << endl;
        // "afegir text ..." i "afegir cita x y"
		if (op == "afegir"){
			iss >> op;
			if (op == "text") {
				string titol;			
				ws(iss);
                char c = iss.peek();
                getline(iss, titol);
				if (c == '"' and titol[titol.size() - 1] == '"') {
					string nova_lin;
					getline(cin, nova_lin);
					istringstream iss_s(nova_lin);
					iss_s >> op;
					if (op == "autor") {				
						string autor;
						ws(iss_s);
						char c=iss_s.peek(); 
                        getline(iss_s, autor);
						if (autor.size() > 2 and c == '"' and autor[autor.size() - 1] == '"' and
							son_tot_paraules(autor.substr(1, autor.size() - 2))) {
							string text = "";
							bool first = true;
							getline(cin, nova_lin);
							while (nova_lin != "****") {
								if (first)
									first = false;
								/*else
									text += " ";*/
								text += nova_lin;
								text += " ";
								getline(cin, nova_lin);
							}
							// Treiem cometes
                            gestor.afegir_text(
                            	autor.substr(1, autor.size() - 2),
                            	titol.substr(1, titol.size() - 2), 
                            	text, sortida);
                            
						}
					}
				}
			}
			else if(op == "cita") {
                ws(iss);
                char c = iss.peek();
                bool es_negatiu_primer = c == '-';
                if (es_negatiu_primer) {
                    iss.ignore(1, '-');
                    c = iss.peek();
                }
                if (isdigit(c)) {
                    int x, y;
                    iss >> x;
                    if (es_negatiu_primer)
                        x *= -1;
                    ws(iss);
                    c = iss.peek();
                    bool es_negatiu_segon = c == '-';
                    if (es_negatiu_segon) {
                        iss.ignore(1, '-');
                        c = iss.peek();
                    }
                    if (isdigit(c)) {
                        iss >> y;
                        if (es_negatiu_segon)
                            y *= -1;
                        ws(iss);
                        c = iss.peek();
                        if (isdigit(c) or c == -1) {
                            gestor.afegir_cita(x, y, sortida);
                        }
                    }
                }
			}
        }
        // "eliminar text" i "eliminar cita ref"
		else if (op=="eliminar") {
			string op;
			iss >> op;
			if (op == "text") {
				ws(iss);
				if (iss.peek() == -1)
                	gestor.eliminar_text(sortida);
			} else if (op == "cita") {
				string ref;
				ws(iss);
				char ci = iss.peek(); 
				iss >> ref;
				// La referència de la cita a eliminar ha d'estar entre cometes "".
				if (ci == '"' and ref.size() > 3 and ref[ref.size() - 1] == '"' and 
						es_unica_paraula(ref.substr(1, ref.size() - 2))) {
                    gestor.eliminar_cita(
                    	ref.substr(1, ref.size() - 2), 
                    	sortida);
				}
			}
        }
        // "substitueix p1 per p2"
		else if (op=="substitueix")	{
			string p1;
			ws(iss);
			char c=iss.peek(); 
			iss >> p1;
			// La paraula substituida ha de estar entre cometes "".
            if (c == '"' and p1.size() > 2 and p1[p1.size() - 1] == '"') {
				iss >> op;
				if (op == "per") {
					string p2;
					ws(iss);
					char d=iss.peek();
					iss >> p2;
					// La paraula que substituirà l'anterior també ha de estar entre cometes "".
                    if (d == '"' and p2.size() > 2 and p2[p2.size() - 1] == '"' and 
                    	es_unica_paraula(p1.substr(1, p1.size() - 2)) and 
                    	es_unica_paraula(p2.substr(1, p2.size() - 2))) {

                        gestor.substitueix(
                        	p1.substr(1, p1.size() - 2), 
                        	p2.substr(1, p2.size() - 2), 
                        	sortida);
					}
				} 
			}
		}
        // "triar text {p1 p2 .. pn}"
		else if (op=="triar") {
			iss >> op;
			if (op == "text") {				
				string paraules;
				ws(iss);	
				char c=iss.peek();
				getline(iss,paraules);
				// El text triat ha d'estar entre claudàtors {}.
				if (paraules.size() > 2 and c == '{' and paraules[paraules.size() - 1] == '}' and
					son_tot_paraules(paraules.substr(1, paraules.size() -2))) {
                    gestor.triar_text(
                    	paraules.substr(1, paraules.size() -2), 
                    	sortida);
				}				
			}			
        }
        // "textos autor ... ?"
		else if (op=="textos") {
			iss >> op;
			if (op == "autor") {
				string autor;
				ws(iss);	
				char c=iss.peek();
				getline(iss,autor);
				// El autor del text ha d'estar entre cometes "".
                if (c == '"' and autor.size() > 4 and autor[autor.size() - 1] == '?' and
                        autor[autor.size() - 3] == '"' and 
                        son_tot_paraules(autor.substr(1, autor.size() - 4))) {
                    gestor.textos_autor(
                    	autor.substr(1, autor.size() - 4),
                    	sortida);
				}
			}
		}
        // "tots textos ?"
		else if (op=="tots") {
			iss >> op;
			if (op == "textos") {
				iss >> op;
				if (op == "?") {	
                    gestor.tots_textos(sortida);
				}
			}
			else if (op == "autors") {
				iss >> op;
				if (op == "?") {
                    gestor.tots_autors(sortida);
				}
			}
		}
        // "info ?" i "info cita ref ?"
		else if (op == "info") {
			ws(iss);
			char c=iss.peek(); 
			iss >> op;
			if (c == '?') {
                gestor.info(sortida);
			} 
			else if (op == "cita") {
				string ref;
				ws(iss);
				char c=iss.peek(); 
                getline(iss,ref);
				// La referència de la cita a mostrar ha d'estar entre cometes "".
                if (c == '"' and ref.size() > 4 and ref[ref.size() - 3] == '"' and ref[ref.size() - 1] == '?'
                	and es_unica_paraula(ref.substr(1, ref.size() - 4))) {
                    gestor.info_cita(
                    	ref.substr(1, ref.size() - 4),
                    	sortida);
				}
			}
        }
        // "autor ?"
		else if (op == "autor") {
			iss >> op;
			if (op == "?") {
                gestor.autor(sortida);
			}					
        }
        // "contingut ?"
		else if (op == "contingut") {
			iss >> op;
			if (op == "?") {
                gestor.contingut(sortida);
			}
		}
        // "frases (expr) ?" , "frases ... ?" i "frases x y ?"
		else if (op == "frases") {
			string expresio;
			ws(iss);	
            char c = iss.peek();
			getline(iss,expresio);
			// L'expressió a evaluar ha d'estar entre parèntesis ().
            if ((c == '(' or c == '{') and expresio.size() >= 4 and expresio[expresio.size() - 1] == '?'
                    and (expresio[expresio.size() - 3] == ')' or expresio[expresio.size() - 3] == '}')) {
                expresio.erase(expresio.end() - 2, expresio.end());
                gestor.frases_expr(expresio,sortida);
			} 
			// La informació de la cita referenciada ha d'estar entre cometes "".
            else if (c == '"' and expresio.size() > 4 and expresio[expresio.size() - 3] == '"'
                     and expresio[expresio.size() - 1] == '?' and 
                     son_tot_paraules(expresio.substr(1, expresio.size() - 4))) {
                gestor.frases_frase(
                	expresio.substr(1, expresio.size() - 4),
                	sortida);
			}
			// La frase entre la x-èsima i la y-èsima han de ser enters
			else {
                istringstream iss_s(expresio);
                bool es_negatiu_primer = c == '-';
                if (es_negatiu_primer) {
                    iss_s.ignore(1, '-');
                    c = iss_s.peek();
                }
                if (isdigit(c)) {
                    int x, y;
                    iss_s >> x;
                    if (es_negatiu_primer)
                        x *= -1;
                    ws(iss_s);
                    c = iss_s.peek();
                    bool es_negatiu_segon = c == '-';
                    if (es_negatiu_segon) {
                        iss_s.ignore(1, '-');
                        c = iss_s.peek();
                    }
                    if (isdigit(c)) {
                        iss_s >> y;
                        if (es_negatiu_segon)
                            y *= -1;
                        ws(iss_s);
                        string par;
                        iss_s >> par;
                        if (par.size() == 1 and par[0] == '?') {
                            gestor.frases(x, y, sortida);
                        }
                    }
                }
			}
		}
        // "nombre de frases ?" i "nombre de paraules ?"
		else if (op == "nombre") {
			iss >> op;
			if (op == "de") {				
				iss >> op;		
				if (op == "frases") {
					iss >> op;			
					if (op == "?") {
                        gestor.nombre_frases(sortida);
					}
				}
				else if (op == "paraules") {
					iss >> op;			
					if (op == "?") {
                        gestor.nombre_paraules(sortida);
					}
				}
			}
		}
        // "taula de frequencies ?"
		else if (op == "taula") {
			iss >> op;
			if (op == "de") {
				iss >> op;
				if (op == "frequencies") {
					iss >> op;			
					if (op == "?") {
                        gestor.taula_frequencies(sortida);
					}
				}
			}
		}
        // "cites ?" i "cites autor ... ?"
		else if (op == "cites") {
			ws(iss);
            char c=iss.peek();
			iss >> op;
            if (op.size() == 1 and c == '?') {
				gestor.cites_text(sortida);
			} 
			else if (op == "autor") {
				string autor;
				ws(iss);	
				char c=iss.peek();
				getline(iss,autor);
				// L'autor del qual volem saber totes les seves cites ha d'estar entre cometes "".
                if (c == '"' and autor.size() > 5 and autor[autor.size() - 3] == '"'
                        and autor[autor.size() - 1] == '?' and son_tot_paraules(autor.substr(1, autor.size() - 4))) {
                    gestor.cites_autor(
                    	autor.substr(1, autor.size() - 4),
                    	sortida);
				}
			}
		}
        // "totes cites ?"
		else if (op == "totes") {
			iss >> op;
			if (op == "cites") {
				iss >> op;
				if (op == "?") {
                    gestor.totes_cites(sortida);
				}
			}
		}
        cout << sortida;
		getline(cin,linia);
        if (sortida != "")
            cout << endl;

	}
}
