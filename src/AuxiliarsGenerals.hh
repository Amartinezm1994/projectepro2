#include <string>
#include "Arbre.hh"
#include <vector>
#include <set>

using namespace std;

/** @brief Diu si el carácter és una lletra o un número.
    \pre <em>cert</em>
    \post retorna verdader si el caràcter és una lletra o un número.
*/
bool es_alfanumeric(char c);

/** @brief Diu si la inicial d'aquesta paraula pertany a una paraula real.
 *  \pre <em>cert</em>
 *  \post si el retorn és cert llavors la inicial de p pertany a la
 *      de una paraula real, altrament si el retorn és fals llavors no
 *      pertany a la de una paraula real.
*/
bool es_inicial_paraula(const string &p);

/** @brief Diu si el caràcter és un signe de puntuació vàlid.
 *  \pre <em>cert</em>
 *  \post si el retorn és cert llavors c és un signe de puntuació vàlid,
 *      altrament, si el retorn és fals no ho és.
*/
bool es_puntuacio(char c);

/** @brief Funció d'immersió per crear un arbre d'expressions boleanes amb
 *      paraules a les fulles.
    \pre a és un arbre buit, i = I, i és l'índex següent de '(' a str.
    \post si el retorn és cert llavors a és un arbre d'expressions i s'ha
        avançat fins el següent ')', altrament, si el retorn és fals llavors
        str és una expressió invàlida.
*/
bool i_crea_arbre(Arbre<string> &a, const string &str, int &i);

/** @brief Crea un arbre d'expresions extretes d'un interval d'un
 *      string on només conté paraules.
    \pre a és un arbre buit, i = I, i apunta al següent caràcter després
        de '{'.
    \post si el retorn és cert llavors a és un arbre de nodes connectats
        per arrels '&' i i s'ha augmentat fins a la posició on es troba
        el següent '}' a str, altrament, si el retorn és fals llavors
        l'interval entre str[I] i str[i] és invàlid com a seqüència de
        nodes terminats en '}'.
*/
bool crea_arbre_nodes(Arbre<string> &a, const string &str, int &i);

/** @brief Avança i fins al següent caràcter diferent d'un espai.
    \pre i = I
    \post i queda avançada fins el següent caràcter que no sigui espai
        des de I fins a i.
*/
void treu_espais(const string &str, int &i);


/** @brief Imprimeix l'arbre inordre.
 *  \pre a = A
 *  \post Deixa al canal de sortida el contingut de les arrels de
 *      l'arbre en ordre inordre, i a = A
*/
void imprimeix_arbre(Arbre<string> &a);

/** @brief Crea un arbre binari amb totes les expresions contenides a expr.
    \pre a és buit.
    \post si el retorn és cert a és l'arbre d'expresions de expr, altrament
        el retorn és fals quan la expressió no està ben formada.
*/
bool crea_arbre(Arbre<string> &a, const string &expr);


/** @brief Evalua la cadena de caràcters per veure si hi han paraules en comú.
    \pre v és el vector amb les paraules a comparar, cadena és una agrupació de
        paraules d'on es compara v, exist és un vector de boleans el qual
        ha d'estar prèviament inicialitzat i correspon amb v i indica les paraules
        ja trobades, i trobades és el nombre de paraules trobades.
    \post exist s'ha actualitzat amb les paraules trobades a cadena i trobades
        és el nombre actual de paraules trobades.
*/
void analitza_paraula(const vector<string> &v, vector<bool> &exist,
    const string &cadena, int &trobades);

/** @brief suprimeix tots els números de la dreta fins arribar a un caràcter.
    \pre str és una referència vàlida.
    \post num conté el número de la part dreta de str i ref conté tota la resta.
*/
void desglosa_referencia(const string &str, string &ref, int &num);

/** @brief Normalitza una cadena de text donada en format, paraula espai paraula...
 * \pre str només conté paraules vàlides i str és diferent a nou.
 * \post nou és la nova cadena de caràcters normalitzada de str, si to_mayus es cert llavors
 *      la primera lletra de cada paraula serà mayúscula, altrament no s'alteren.
*/
void normalitza_cadena(const string &str, string &nou);

/** @brief Un títol normalitzat és un títol amb un sol espai entre paraules, i
    signes de puntuació enganxats a l'última paraula.
    \pre str no conté signes de puntuació consecutius.
    \post nou és un títol normalitzat.
*/
void normalitza_titol(const string &str, string &nou);

/** @brief Obté el conjunt de paraules de str sense repetir cap.
    \pre str conté paraules vàlides i sense signes de puntuació.
    \post s conté el conjunt de paraules extret de str sense repetir cap.
*/
void obtindre_conjunt_cadena(const string &str, set<string> &s);

/** \pre <em>cert</em>
    \post si el retorn és cert, str és un conjunt
        de paraules separades per un espai, cada
        paraula noméx conté lletres o números, altrament
        el retorn és fals.
*/
bool son_tot_paraules(const string &str);

/** \pre <em>cert</em>
    \post si el retorn és cert, str és una paraula entre espais
        o tabulacions, altrament, el retorn és fals.
*/
bool es_unica_paraula(const string &str);
