/** @file Cita.hh
    @brief Especificació de la classe Cita
*/
#ifndef CITA_HH
#define CITA_HH

#include <vector>
#include <string>

using namespace std;
/** @class Cita
    @brief Representa una cita d'un text i autor determinat.
*/
class Cita {
private:
	/** @brief autor és el nom de l'autor de la cita del text. */
    string autor;
    /** @brief titol és el títol del text de la cita. */
    string titol;
    /** @brief frases és el conjunt des de la xfrase-éssima a la xfrase-éssima + mida del vector
    	frases del text de la cita. */
	vector<string> frases;
	/** @brief indica el número inicial de frase del text de la cita. */
	int xessima;

	/* Invariant:
		- autor és l'autor del text de la cita.
		- titol és el títol del text de la cita.
		- frases són les frases del text des de la xfrase-éssima 
			fins a la xfrase-éssima + mida del vector independentment
			si es modifiquen al text original.
	*/

public:
	/** @brief Constructor de la classe Cita.
		\pre <em>cert</em>
		\post es crea la cita amb l'autor, títol i frases entrades a l'argument. */
	Cita(const string &autor, const string &titol,
		int xfrase, vector<string> &frases);
	~Cita();

	// OPERACIONS DE CONSULTA (no modifiquen el p.i.)

	/** @brief Diu l'autor al que correspon la cita.
		\pre <em>cert</em>
		\post autor és l'autor del text de la cita.
	*/
    void autor_cita(string &autor) const;

	/** @brief Diu el títol del text al que correspon la cita.
		\pre <em>cert</em>
		\post titol és el títol del text de la cita.
	*/
    void titol_cita(string &titol) const;

	/** @brief Dóna totes les frases d'aquesta cita.
		\pre <em>cert</em>
		\post frases són les frases emmagatzemades al p.i.
	*/
    void frases_cita(vector<string> &frases) const;

    /** @brief Dóna totes les frases d'aquesta cita.
		\pre <em>cert</em>
		\post el retorn són les frases emmagatzemades al p.i. amb
			per cada línia el seu número de frase en el text seguit de la frase.
	*/
    void frases_cita_str(string &sortida) const;

	/** @brief Diu la frase x-éssima a la que correspon la cita en el text.
		\pre <em>cert</em>
		\post retorna el nombre de la frase inicial de la cita.
	*/
    int xfrase() const;

	/** @brief Diu la frase y-éssima a la que correspon la cita en el text.
		\pre <em>cert</em>
		\post retorna el nombre de la frase final de la cita.
	*/
    int yfrase() const;

};
#endif