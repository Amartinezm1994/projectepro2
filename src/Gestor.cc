#include <map>
#include <string>
#include <sstream>
#include <cmath>

#include "Gestor.hh"
#include "AuxiliarsGenerals.hh"

using namespace std;

const string Gestor::MISSATGE_ERROR = "error";

bool Gestor::obtindre_frases(int x, int y, string &sortida, 
                    const map<string,Autor>::const_iterator &it) const
{
    return it->second.frases(x, y, sortida, triat_text);
}

void Gestor::afegeix_info_cita(bool autor, string &info, const map<int,Cita>::const_iterator &it, const string &ref_name) const
{
	info += ref_name;
	info += to_string(it->first);
	info += "\n";
	string aux;
	it->second.frases_cita_str(aux);
	info += aux;
	info += "\n";
	if (autor) {
		it->second.autor_cita(aux);
		info += aux;
		info += " ";
	}
	info += "\"";
	it->second.titol_cita(aux);
	info += aux;
	info += "\"";
}


Gestor::Gestor()
{
	autors = map<string,Autor>();
	cites = map<string,map<int,Cita> >();
	recompte_referencies = map<string,int>();
	hi_ha_triat = false;
}

Gestor::~Gestor() {}

void Gestor::afegir_text(
	const string &autor, const string &titol,
	string &text, string &sortida)
{
	bool afegit = true;
    string autor_norm;
    normalitza_cadena(autor, autor_norm);
    string titol_norm;
    normalitza_titol(titol, titol_norm);
    map<string,Autor>::iterator it = autors.find(autor_norm);
	if (it != autors.end()) {
		afegit = it->second.afegir_text(titol_norm, text);
	} else {
        Autor aut(autor_norm, titol_norm, text);
        it = autors.insert(make_pair(autor_norm, aut)).first;
		afegit = true;
	}
	if (afegit) {
		sortida = "";
		string ref = "";
        istringstream iss(autor_norm);
		char last = iss.peek();
		while(last != -1) {
			string aux;
			iss >> aux;
			ref += last;
			ws(iss);
			last = iss.peek();
		}
		map<string,map<int,Cita> >::const_iterator it_c = cites.find(ref);
		if (it_c != cites.end()) {
			for (map<int,Cita>::const_iterator it_i = it_c->second.begin(); 
				it_i != it_c->second.end(); ++it_i) {
				string autor_c, titol_c;
				it_i->second.autor_cita(autor_c);
				it_i->second.titol_cita(titol_c);
                if (autor_c == autor_norm and titol_c == titol_norm) {
					it->second.afegeix_cita(make_pair(it_c->first, it_i->first), titol_norm);
				}
			}
		}
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::eliminar_text(string &sortida)
{
	if (hi_ha_triat) {
		map<string,Autor>::iterator it = autors.find(triat_autor);
		it->second.eliminar_text(triat_text);
		if (not it->second.hi_ha_textos() and
			not it->second.hi_han_cites())
			autors.erase(it);
		hi_ha_triat = false;
	} else 
		sortida = MISSATGE_ERROR;
}

void Gestor::afegir_cita(int x, int y, string &sortida)
{	
	if (hi_ha_triat) {
		map<string,Autor>::iterator it = autors.find(triat_autor);
		bool rang_correcte = it->second.frases_rang_correcte(x, y, triat_text);
		if (rang_correcte) {
			set<pair<string,int> >::const_iterator begin, end;
			it->second.refs_text(begin, end, triat_text);
			bool igual_cita = false;
			map<string,map<int,Cita> >::const_iterator it_c;
			if (begin != end)
				it_c = cites.find(begin->first);
			while(not igual_cita and begin != end) {
				map<int,Cita>::const_iterator it_n = it_c->second.find(begin->second);
				igual_cita = it_n->second.xfrase() == x and it_n->second.yfrase() == y;
				++begin;
			}
			
			if (not igual_cita) {
				vector<string> v;
				it->second.frases_vector(x, y, v, triat_text);
				istringstream iss(triat_autor);
				string ref = "";
				char last = iss.peek();
				while(last != -1) {
					string aux;
					iss >> aux;
                    ref += last;
					ws(iss);
					last = iss.peek();
				}
				map<string,int>::iterator it_rec = recompte_referencies.find(ref);
				int num_ref;
				if (it_rec != recompte_referencies.end()) {
					it_rec->second++;
					num_ref = it_rec->second;
				} else {
					recompte_referencies.insert(make_pair(ref, 1));
					num_ref = 1;
				}
				Cita cita(triat_autor, triat_text, x, v);
				map<string,map<int,Cita> >::iterator it_ref = cites.find(ref);
				if (it_ref == cites.end()) {
					it_ref = cites.insert(make_pair(ref, map<int,Cita>())).first;
				}
				it_ref->second.insert(make_pair(num_ref, cita));
				it->second.afegeix_cita(make_pair(ref, num_ref), triat_text);
			} else
				sortida = MISSATGE_ERROR;
		} else
			sortida = MISSATGE_ERROR;
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::eliminar_cita(const string &ref, string &sortida)
{
	string ref_name;
	int ref_num;
    desglosa_referencia(ref, ref_name, ref_num);
	map<string,map<int,Cita> >::iterator it = cites.find(ref_name);
	if (it != cites.end()) {
		map<int,Cita>::iterator it_num = it->second.find(ref_num);
		if (it_num != it->second.end()) {
			string autor;
			it_num->second.autor_cita(autor);
			string titol;
			it_num->second.titol_cita(titol);
			map<string,Autor>::iterator it_a = autors.find(autor);
			if (it_a != autors.end()) {
				it_a->second.eliminar_cita(make_pair(ref_name, ref_num), titol);
				if (not it_a->second.hi_ha_textos() and
					not it_a->second.hi_han_cites()) {
					autors.erase(it_a);
				}
			}
			it->second.erase(it_num);
			if (it->second.empty())
				cites.erase(it);
		} else
			sortida = MISSATGE_ERROR;
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::textos_autor(const string &autor, string &sortida) const
{
	sortida = "";
    string autor_norm;
    normalitza_cadena(autor, autor_norm);
    map<string,Autor>::const_iterator it = autors.find(autor_norm);
    if (it != autors.end() and it->second.hi_ha_textos()) {
		it->second.titols_textos(sortida);
	}
}

void Gestor::tots_textos(string &sortida) const
{
	if (!autors.empty()) {
		map<string,Autor>::const_iterator it = autors.begin();
		sortida = "";
		bool first = true;
		while(it != autors.end()) {
            if (it->second.hi_ha_textos()) {
                if (first)
                    first = false;
                else
                    sortida += "\n";
                string textos;
                it->second.autor_titols_textos(textos);
                sortida += textos;
            }
			++it;
		}
	}
}


void Gestor::substitueix(const string &p1, const string &p2, string &sortida) 
{
	if (hi_ha_triat) {
		if (p1 != p2) {
	        map<string,Autor>::iterator it = autors.find(triat_autor);
            it->second.substitueix(p1, p2, triat_text);
	    }
	} else 
		sortida = MISSATGE_ERROR;
}

void Gestor::triar_text(const string &paraules, string &sortida)
{
	set<string> ss;
	obtindre_conjunt_cadena(paraules, ss);
	vector<string> v(ss.size());
	int i = 0;
	set<string>::const_iterator it_set = ss.begin();
	while(i < v.size()) {
		v[i] = *(it_set);
		++i;
		++it_set;
	}
	map<string,Autor>::iterator it = autors.begin();
	int estat_cerca = -1;
    string triat_text;
	while(estat_cerca != 0 and it != autors.end())
	{
        if (it->second.hi_ha_textos()) {
            int actual = it->second.selecciona_text(v, triat_text);
            if ((actual == 1 and estat_cerca == 1) or actual == 0) {
                estat_cerca = 0;
            } else {
                if (actual == 1) {
                    triat_autor = it->first;
                    this->triat_text = triat_text;
                    estat_cerca = 1;
                }
            }
        }
		++it;
	}
	if (estat_cerca == 0 or estat_cerca == -1) {
		sortida = MISSATGE_ERROR;
		hi_ha_triat = false;
	} else 
		hi_ha_triat = true;
}

void Gestor::tots_autors(string &sortida) const
{
	sortida = "";
	bool first = true;
	for (map<string,Autor>::const_iterator it = autors.begin();
		it != autors.end(); ++it) 
	{
		if (it->second.hi_ha_textos()) {
			if (first)
				first = false;
			else
				sortida += '\n';
			sortida += it->first;
			sortida += " ";
			sortida += to_string(it->second.nombre_textos());
			sortida += " ";
			sortida += to_string(it->second.nombre_frases());
			sortida += " ";
			sortida += to_string(it->second.nombre_paraules());
			
		}
	}
}

void Gestor::info(string &sortida) const
{
	if (hi_ha_triat) {
		map<string,Autor>::const_iterator it = autors.find(triat_autor);
		int num_paraules, num_frases;
		set<pair<string,int> >::const_iterator begin, end;
		it->second.refs_paraules_frases_text(begin, end, 
			num_frases, num_paraules, triat_text);
        sortida = triat_autor;
        sortida += " \"";
        sortida += triat_text;
        sortida += "\" ";
        sortida += to_string(num_frases);
        sortida += " ";
        sortida += to_string(num_paraules);
        sortida += "\n";

        map<string,map<int,Cita> >::const_iterator it_c;
        if (begin != end) {
            it_c = cites.find(begin->first);

        }
        sortida += "Cites Associades:";
        while(begin != end) {
            vector<string> frases;
            map<int,Cita>::const_iterator it_cites = it_c->second.find(begin->second);
            it_cites->second.frases_cita(frases);
            sortida += "\n";
            sortida += begin->first;
            sortida += to_string(begin->second);
            for (int i = 0; i < frases.size(); ++i) {
                sortida += "\n";
                sortida += to_string(i + it_cites->second.xfrase());
                sortida += " ";
                sortida += frases[i];
            }
            ++begin;
			}
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::autor(string &sortida) const
{
	if (hi_ha_triat) {
		sortida = triat_autor;
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::contingut(string &sortida) const
{
	if (hi_ha_triat) {
        map<string,Autor>::const_iterator it = autors.find(triat_autor);
        int num_frases = it->second.num_frases_triat(triat_text);
		this->obtindre_frases(1, num_frases, sortida, it);
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::frases(int x, int y, string &sortida) const
{
	if (hi_ha_triat) {
        map<string,Autor>::const_iterator it = autors.find(triat_autor);
		bool rang_correcte = this->obtindre_frases(x, y, sortida, it);
		if (not rang_correcte)
			sortida = MISSATGE_ERROR;
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::nombre_frases(string &sortida) const
{
	if (hi_ha_triat) {
        sortida = to_string(autors.find(triat_autor)->second.num_frases_triat(triat_text));
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::nombre_paraules(string &sortida) const
{
	if (hi_ha_triat) {
        sortida = to_string(autors.find(triat_autor)->second.num_paraules_triat(triat_text));
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::taula_frequencies(string &sortida) const 
{
	if (hi_ha_triat) {
		sortida = "";
        autors.find(triat_autor)->second.taula_frequencies(sortida, triat_text);
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::frases_expr(const string &expr, string &sortida) const 
{
	if (hi_ha_triat) {
        if (not autors.find(triat_autor)->second.frases_expr(expr, sortida, triat_text))
			sortida = "";
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::info_cita(const string &ref, string &sortida) const 
{
	string ref_a;
	int num_ref;
	desglosa_referencia(ref, ref_a, num_ref);
	map<string,map<int,Cita> >::const_iterator it = cites.find(ref_a);
	if (it != cites.end()) {
		map<int,Cita>::const_iterator it_n = it->second.find(num_ref);
		if (it_n != it->second.end()) {
			string aux;
			it_n->second.autor_cita(aux);
			sortida += aux;
			sortida += " \"";
			it_n->second.titol_cita(aux);
			sortida += aux;
			sortida += "\"\n";
			sortida += to_string(it_n->second.xfrase());
			sortida += '-';
			sortida += to_string(it_n->second.yfrase());
			sortida += "\n";
			it_n->second.frases_cita_str(aux);
			sortida += aux;
		} else
			sortida = MISSATGE_ERROR;
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::cites_autor(const string &autor, string &sortida) const 
{
    string autor_norm;
    normalitza_cadena(autor, autor_norm);
    map<string,Autor>::const_iterator it_a = autors.find(autor_norm);
	sortida = "";
	if (it_a != autors.end()) {
		set<pair<string,int> >::const_iterator begin, end;
		it_a->second.refs_autor(begin, end);
		map<string,map<int,Cita> >::const_iterator it;
		bool first = true;
		if (begin != end)
			it = cites.find(begin->first);
		while (begin != end) {
			if (first)
				first = false;
			else
				sortida += "\n";
			afegeix_info_cita(false, sortida, it->second.find(begin->second), begin->first);
			++begin;
			
		}
	}
}

void Gestor::cites_text(string &sortida) const 
{
	if (hi_ha_triat) {
		sortida = "";
		set<pair<string,int> >::const_iterator begin, end;
		autors.find(triat_autor)->second.refs_text(begin, end, triat_text);
		map<string,map<int,Cita> >::const_iterator it;
		bool diferent = begin != end;
		bool first = true;
		if (diferent)
			it = cites.find(begin->first);
		while(begin != end) {
			if (first)
				first = false;
			else
				sortida += "\n";
			afegeix_info_cita(true, sortida, it->second.find(begin->second), begin->first);
			++begin;
		}
	} else
		sortida = MISSATGE_ERROR;
}

void Gestor::totes_cites(string &sortida) const 
{
	sortida = "";
	bool first = true;
	for (map<string,map<int,Cita> >::const_iterator it_i = cites.begin();
		it_i != cites.end(); ++it_i)
	{
		for (map<int,Cita>::const_iterator it_j = it_i->second.begin();
			it_j != it_i->second.end(); ++it_j) 
		{

			if (first)
				first = false;
			else
				sortida += "\n";
			afegeix_info_cita(true, sortida, it_j, it_i->first);
			
		}
	}
}



void Gestor::frases_frase(const string &fr, string &sortida) const 
{
	if (hi_ha_triat) {
		autors.find(triat_autor)->second.frases_frase(fr, sortida, triat_text);
	} else
		sortida = MISSATGE_ERROR;
}
