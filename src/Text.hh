/** @file Text.hh
    @brief Especificació de la classe Text
*/
#ifndef TEXT_HH
#define TEXT_HH
#include <map>
#include <vector>
#include <string>
#include <set>

#include "Frase.hh"
#include "Arbre.hh"

/** @class Text
    @brief Representa un text el qual té informació sobre aquest.
*/
class Text {

private:
    struct ComparacioParaules {
        /** @brief Diu si p1 és més curta que p2, en cas d'empat
         *      diu si p1 és més petita que p2 en ordre alfabétic.
            \pre <em>cert</em>
            \post si el retorn és cert, p1 és més curta que p2 o p1 és de la mateixa
                mida que p2 i p1 és més petita alfabéticament que p2, altrament
                si el retorn és fals, p2 és més curta que p1 o p1 i p2 són de
                igual mida però p2 és més petita alfabèticament.
        */
        bool operator() (const string &p1, const string &p2) const;
    };

    /** @brief titol és el títol d'aquest text. */
    string titol;
    /** @brief autor és l'autor d'aquest text. */
    string autor;
    /** @brief frases són les frases numerades per l'índex de la posició del vector i
        cada vector de cada frase és el conjunt de paraules, una posició al vector
        és o una paraula o un signe de puntuació o bé un espai. */
    vector<Frase> frases;
    /** @brief nombre_paraules és el nombre de paraules total d'aquest text. */
    int num_paraules;
    /** @brief frequencies és una taula ordenada per ordre decreixent de freqüències
            sense repetir cap paraula i en cas d'empat de freqüència, ordenades
            per ordre creixent segons llargada i després alfabèticament. */
    map<int,set<string,ComparacioParaules> > frequencies;
    /** @brief cites conté totes les referències de les cites d'aquest text ordenades
            per ordre alfabètic. */
    set<pair<string,int> > cites;

    /* Invariant:
        - titol és el títol d'aquest text
        - autor és l'autor d'aquest text
        - frases conté totes les frases que formen el text acabades en '.' o
            '!' o bé '?', l'índex del vector és la frase i-éssima + 1 en el text
        - nombre_paraules és el nombre total de paraules que formen el text
        - frequencies és una taula amb totes les paraules emmagatzemades a frases,
            la seva freqüència i de manera identificativa, no es repeteix cap paraula, 
            frequencies està ordenat decreixentment per freqüència i en cas d'empat les 
            paraules s'ordenen creixentment, primer per llargada i després 
            alfabèticament.
        - cites són les referències a les cites que corresponen a aquest text
    */

    /** @brief Afegeix una aparició d'aquesta paraula a la taula de freqüències.
        \pre <em>cert</em>
        \post frequencies conté una aparició més de p.
    */
    void afegir_paraula_taula_frequencies(const string &p);

    /** @brief Diu si la frase donada compleix l'expresió.
        \pre a = A, a és necessàriament un arbre binari d'expressions a avaluar
            vàlid i no buit, fr és la frase a avaluar.
        \post a és l'arbre inicial A, si el retorn és cert llavors fr compleix
            amb l'expressió d'A, altrament, si el retorn és fals no la compleix.
    */
    static bool avalua_frase_arbre(Arbre<string> &a, const Frase &fr);

    /** @brief Si la paraula és una paraula vàlida dintre del context d'un text, llavors
     *      classifica la paraula en una de les 5 variants, paraula amb signe de puntuació,
     *      paraula amb dos signes de puntuació, un signe de puntuació, dos signes de
     *      puntuació o paraula normal, arrel d'això la fica en la frase, i conta a la
     *      taula de freqüències si és una paraula en aquest cas, incrementa el contador de
     *      paraules, actualitza istringstream per donar pas a la següent paraula. Aquesta és
     *      una funció auxiliar d'afegir text.
     *  \pre la mida de paraula és més gran que 0, iss està ja inicialitzat amb una string.
     *  \post en cas que paraula sigui una paraula normal llavors a fr s'afegeix paraula,
     *      s'afegeix a frequencies la paraula i s'augmenta el contador num_paraules, si
     *      la paraula contè un o dos signes de puntuació, es fa el mateix que si fos una
     *      paraula normal i a més s'afegeix a fr aquest signe de puntuació darrere d'aquesta
     *      paraula, en aquests tres casos, s'afegeix un espai en blanc abans d'afegir la paraula
     *      i el signe de puntuació. Si paraula és un signe de puntuació o un doble signe de
     *      puntuació, a fr s'afegeix aquests dos signes de puntuació, en el cas del doble
     *      signe s'afegeix continuat.
    */
    void gestiona_paraula(string &paraula, Frase &fr, istringstream &iss);

public:
    /** @brief Constructor de la classe text.
        \pre <em>cert</em>
        \post es crea el text amb el contingut de text i amb el títol, i inicialitza
            el p.i.
    */
    Text(string &text, const string &titol, const string &autor);
    ~Text();


    // OPERACIONS D'ESCRIPTURA (modifiquen el p.i.)

    /** @brief Substitueix totes les aparicions d'una paraula de l'últim text triat
        per altre.
        \pre p1 i p2 han de ser diferents.
        \post canvia totes les aparicions de p1 per p2 en el contingut d'aquest text.
    */
    void substitueix(const string &p1, const string &p2);

    /** @brief Afegeix la referència de la cita en aquest text.
        \pre <em>cert</em>
        \post si no existeix la referència d'aquesta cita s'afegeix la referència de 
            la cita al conjunt de referències de les cites i el retorn és cert, altrament
            el retorn és fals.
    */
    void afegeix_cita(const pair<string,int> &p);

    /** @brief Elimina la referència de la cita d'aquest text.
        \pre <em>cert</em>
        \post si existeix la referència d'aquesta cita llavors s'elimina aquesta de
            les cites d'aquest text i el retorn és cert, altrament no s'elimina res.
    */
    void eliminar_cita(const pair<string,int> &p);


    // OPERACIONS DE CONSULTA (no modiquen el p.i.)

    /** @brief Diu el nombre total de paraules d'aquest text.
        \pre <em>cert</em>
        \post retorna el nombre de paraules d'aquest text.
    */
    int nombre_paraules() const;

    /** @brief Diu el nombre total de frases d'aquest text.
        \pre <em>cert</em>
        \post retorna el nombre de frases d'aquest text.
    */
    int nombre_frases() const;

    /** @brief Comprova si les paraules hi són al text.
        \pre <em>cert</em>
        \post si les paraules de v són al text, el valor de retorn és cert,
            altrament serà fals.
    */
    bool hi_son_paraules(const vector<string> &v) const;

    /** @brief Dóna les frases des de la x-éssima a la y-éssima frase del text.
        \pre 0 < xfrase <= yfrase <= nombre_frases()
        \post frases són les frases des de la x-éssima frase a la y-éssima frase d'aquest text.
    */
    void frases_x_y(int xfrase, int yfrase, string &frases) const;

    /** @brief Dóna les frases des de la x-éssima a la y-éssima frase del text.
        \pre 0 < xfrase <= yfrase <= nombre_frases()
        \post v és el vector de la x-éssima frase a la y-éssima frase d'aquest text.
    */
    void frases_x_y_vector(int xfrase, int yfrase, vector<string> &frases) const;

    /** @brief Dóna la taula de freqüències d'aquest text.
        \pre <em>cert</em>
        \post taula conté la taula de freqüències per ordre descendent per freqüència,
            en cas d'empat s'ordenen creixentment per llargada de paraula i després
            alfabéticament.
    */
    void taula_frequencies(string &taula) const;

    /** @brief Dóna les frases que compleixin l'avaluació de l'expressió.
        \pre <em>cert</em>
        \post si el retorn és cert, frases conté les frases numerades que compleixen 
            l'expressió del text, si el retorn és fals, expr està mal formada. 
    */
    bool frases_expr(const string &expr, string &frases) const;
    
    /** @brief Dóna les frases que continguin la seqüència de paraules de frase.
        \pre <em>cert</em>
        \post v conté totes les frases que contenen íntegrament frase del text.
    */
    void frases_frase(const string &frase, string &sortida) const;

    /** @brief Dóna els iteradors d'inici i final de les cites emmagatzemades al text.
     * \pre <em>cert</em>
     * \post begin i end són els iteradors d'inici i final del set que emagatzema les refrències de
     *      les llistes.
    */
    void refs_text(set<pair<string,int> >::const_iterator &begin, set<pair<string,int> >::const_iterator &end) const;

};
#endif
