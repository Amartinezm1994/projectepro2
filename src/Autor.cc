#include <map>
#include <set>
#include <string>

#include "Autor.hh"
#include "Text.hh"

using namespace std;

Autor::Autor(const string &autor, const string &titol, string &text)
{
	nom = autor;
	cites = set<pair<string,int> >();
	textos = map<string,Text>();
    num_paraules = 0;
    num_frases = 0;
    afegir_text(titol, text);
}

Autor::~Autor() {}

bool Autor::afegir_text(const string &titol, string &text)
{
	bool no_existeix = textos.end() == textos.find(titol);
	if (no_existeix) {
		Text txt(text, titol, nom);
		textos.insert(make_pair(titol, txt));
		num_paraules += txt.nombre_paraules();
		num_frases += txt.nombre_frases();
	}
	return no_existeix;
}

void Autor::eliminar_text(const string &triat)
{
	map<string,Text>::const_iterator it = textos.find(triat);
	num_paraules -= it->second.nombre_paraules();
	num_frases -= it->second.nombre_frases();
	textos.erase(it);
}

bool Autor::hi_ha_textos() const
{
	return not textos.empty();
}


void Autor::substitueix(const string &p1, const string &p2, const string &triat)
{
    textos.find(triat)->second.substitueix(p1, p2);
}

bool Autor::afegeix_cita(const pair<string,int> &p, const string &titol_text)
{
	set<pair<string,int> >::iterator it = cites.find(p);
	bool afegida = it == cites.end();
	if (afegida) {
		cites.insert(p);
	}
	textos.find(titol_text)->second.afegeix_cita(p);
	return afegida;
}

bool Autor::eliminar_cita(const pair<string,int> &p, const string &titol_text)
{
	set<pair<string,int> >::iterator it = cites.find(p);
	bool eliminada = it != cites.end();
	if (eliminada) {
		cites.erase(it);
		map<string,Text>::iterator itt = textos.find(titol_text);
		if (itt != textos.end()) {
			itt->second.eliminar_cita(p);
		}
	}
	return eliminada;
}

int Autor::selecciona_text(const vector<string> &v, string &trobat)
{
	map<string,Text>::iterator it = textos.begin();
	int estat = -1;
	while(estat != 0 and it != textos.end()) {
		if (it->second.hi_son_paraules(v)) {
			if (estat == 1) 
				estat = 0;
			else {
				estat = 1;
                trobat = it->first;
			}
		}
		++it;
	}
	return estat;
}

bool Autor::hi_han_cites() const
{
	return not cites.empty();
}

bool Autor::existeix_cita(const pair<string,int> &ref) const
{
	return cites.end() != cites.find(ref);
}

int Autor::nombre_textos() const
{
	return textos.size();
}

int Autor::nombre_frases() const
{
	return num_frases;
}

int Autor::nombre_paraules() const
{
	return num_paraules;
}

void Autor::autor_titols_textos(string &titols) const
{
	map<string,Text>::const_iterator it = textos.begin();
	titols = "";
	bool first = true;
	while(it != textos.end()) {
		if (first)
			first = false;
		else
			titols += "\n";
		titols += nom;
		titols += " \"";
		titols += it->first;
		titols += "\"";
		++it;
	}
}

void Autor::titols_textos(string &titols) const
{
	map<string,Text>::const_iterator it = textos.begin();
	titols = "";
	bool first = true;
	while(it != textos.end()) {
		if (first)
			first = false;
		else
			titols += "\n";
		titols += "\"";
		titols += it->first;
		titols += "\"";
		++it;
	}
}

bool Autor::comprova_rang_frases(int xfrase, int yfrase, const string &triat_text, 
        map<string,Text>::const_iterator &it) const 
{
	bool rang_valid = 0 < xfrase and xfrase <= yfrase;
	if (rang_valid) {
		it = textos.find(triat_text);
		rang_valid = it->second.nombre_frases() >= yfrase;
	}
	return rang_valid;
}

bool Autor::frases(int xfrase, int yfrase, string &frases, const string &triat) const
{
	map<string,Text>::const_iterator it;
	bool rang_valid = comprova_rang_frases(xfrase, yfrase, triat, it);
	if (rang_valid) {
		it->second.frases_x_y(xfrase, yfrase, frases);
	}
	return rang_valid;
}

bool Autor::frases_vector(int xfrase, int yfrase, vector<string> &frases, const string &triat) const
{
	map<string,Text>::const_iterator it;
	bool rang_valid = comprova_rang_frases(xfrase, yfrase, triat, it);
	if (rang_valid) {
		it->second.frases_x_y_vector(xfrase, yfrase, frases);
	}
	return rang_valid;
}

bool Autor::frases_rang_correcte(int xfrase, int yfrase, const string &triat)
{
	map<string,Text>::const_iterator it;
	return comprova_rang_frases(xfrase, yfrase, triat, it);
}

int Autor::num_frases_triat(const string &triat) const
{
	return frases_it_text(textos.find(triat));
}

int Autor::num_paraules_triat(const string &triat) const
{
	return paraules_it_text(textos.find(triat));
}

void Autor::taula_frequencies(string &taula, const string &triat) const
{
	textos.find(triat)->second.taula_frequencies(taula);
}

bool Autor::frases_expr(const string &expr, string &frases, const string &triat) const
{
	return textos.find(triat)->second.frases_expr(expr, frases);
}

void Autor::refs_text(set<pair<string,int> >::const_iterator &begin,
                      set<pair<string,int> >::const_iterator &end, const string &triat) const
{
    iteradors_refs_it_text(begin, end, textos.find(triat));
}
void Autor::refs_autor(set<pair<string,int> >::const_iterator &begin,
                       set<pair<string,int> >::const_iterator &end) const
{
    begin = cites.begin();
    end = cites.end();
}

void Autor::refs_paraules_frases_text(set<pair<string,int> >::const_iterator &begin,
                   set<pair<string,int> >::const_iterator &end, int &num_frases, 
                   int &num_paraules, const string &triat) const
{
	map<string,Text>::const_iterator it = textos.find(triat);
	num_paraules = paraules_it_text(it);
	num_frases = frases_it_text(it);
	iteradors_refs_it_text(begin, end, it);
}

int Autor::frases_it_text(const map<string,Text>::const_iterator &it) const
{
	return it->second.nombre_frases();
}

int Autor::paraules_it_text(const map<string,Text>::const_iterator &it) const
{
	return it->second.nombre_paraules();
}

void Autor::iteradors_refs_it_text(set<pair<string,int> >::const_iterator &begin,
                       set<pair<string,int> >::const_iterator &end, 
                       const map<string,Text>::const_iterator &it) const 
{
	it->second.refs_text(begin, end);
}


void Autor::frases_frase(const string &frase, string &sortida, const string &triat) const
{
	textos.find(triat)->second.frases_frase(frase, sortida);
}
